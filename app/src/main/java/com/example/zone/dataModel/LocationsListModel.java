package com.example.zone.dataModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LocationsListModel {

    @SerializedName("result")
    private ResultBean result;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        @SerializedName("user")
        private List<UserBean> user;

        public List<UserBean> getUser() {
            return user;
        }

        public void setUser(List<UserBean> user) {
            this.user = user;
        }

        public static class UserBean {
            @SerializedName("UserUID")
            private int UserUID;
            @SerializedName("RoleID")
            private Object RoleID;
            @SerializedName("EmployeeID")
            private int EmployeeID;
            @SerializedName("ImgUrl")
            private String ImgUrl;
            @SerializedName("FullName")
            private String FullName;
            @SerializedName("User_Name")
            private Object UserName;
            @SerializedName("User_State")
            private Object UserState;
            @SerializedName("Job_L_Nm")
            private Object JobLNm;
            @SerializedName("Is_OutDoor")
            private Object IsOutDoor;
            @SerializedName("Role_Ar_Nm")
            private Object RoleArNm;
            @SerializedName("User_Fcm")
            private Object UserFcm;
            @SerializedName("User_Lat")
            private String UserLat;
            @SerializedName("User_Lng")
            private String UserLng;
            @SerializedName("RollID")
            private Object RollID;
            @SerializedName("Branch_ID")
            private Object BranchID;
            @SerializedName("Branch_Ar_Nm")
            private Object BranchArNm;
            @SerializedName("StoreUID")
            private Object StoreUID;
            @SerializedName("Store_Ar_Nm")
            private Object StoreArNm;

            public int getUserUID() {
                return UserUID;
            }

            public void setUserUID(int UserUID) {
                this.UserUID = UserUID;
            }

            public Object getRoleID() {
                return RoleID;
            }

            public void setRoleID(Object RoleID) {
                this.RoleID = RoleID;
            }

            public int getEmployeeID() {
                return EmployeeID;
            }

            public void setEmployeeID(int EmployeeID) {
                this.EmployeeID = EmployeeID;
            }

            public String getImgUrl() {
                return ImgUrl;
            }

            public void setImgUrl(String ImgUrl) {
                this.ImgUrl = ImgUrl;
            }

            public String getFullName() {
                return FullName;
            }

            public void setFullName(String FullName) {
                this.FullName = FullName;
            }

            public Object getUserName() {
                return UserName;
            }

            public void setUserName(Object UserName) {
                this.UserName = UserName;
            }

            public Object getUserState() {
                return UserState;
            }

            public void setUserState(Object UserState) {
                this.UserState = UserState;
            }

            public Object getJobLNm() {
                return JobLNm;
            }

            public void setJobLNm(Object JobLNm) {
                this.JobLNm = JobLNm;
            }

            public Object getIsOutDoor() {
                return IsOutDoor;
            }

            public void setIsOutDoor(Object IsOutDoor) {
                this.IsOutDoor = IsOutDoor;
            }

            public Object getRoleArNm() {
                return RoleArNm;
            }

            public void setRoleArNm(Object RoleArNm) {
                this.RoleArNm = RoleArNm;
            }

            public Object getUserFcm() {
                return UserFcm;
            }

            public void setUserFcm(Object UserFcm) {
                this.UserFcm = UserFcm;
            }

            public String getUserLat() {
                return UserLat;
            }

            public void setUserLat(String UserLat) {
                this.UserLat = UserLat;
            }

            public String getUserLng() {
                return UserLng;
            }

            public void setUserLng(String UserLng) {
                this.UserLng = UserLng;
            }

            public Object getRollID() {
                return RollID;
            }

            public void setRollID(Object RollID) {
                this.RollID = RollID;
            }

            public Object getBranchID() {
                return BranchID;
            }

            public void setBranchID(Object BranchID) {
                this.BranchID = BranchID;
            }

            public Object getBranchArNm() {
                return BranchArNm;
            }

            public void setBranchArNm(Object BranchArNm) {
                this.BranchArNm = BranchArNm;
            }

            public Object getStoreUID() {
                return StoreUID;
            }

            public void setStoreUID(Object StoreUID) {
                this.StoreUID = StoreUID;
            }

            public Object getStoreArNm() {
                return StoreArNm;
            }

            public void setStoreArNm(Object StoreArNm) {
                this.StoreArNm = StoreArNm;
            }
        }
    }
}
