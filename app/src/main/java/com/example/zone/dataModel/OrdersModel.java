package com.example.zone.dataModel;

import java.io.Serializable;
import java.util.List;

public class OrdersModel {

    private ResultBean result;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        private List<UserBean> user;

        public List<UserBean> getUser() {
            return user;
        }

        public void setUser(List<UserBean> user) {
            this.user = user;
        }

        public static class UserBean implements Serializable{

            private int OrderUID;
            private int OrderSheetID;
            private int ClientID;
            private String ImgUrl;
            private String Client_L_Nm;
            private String Client_F_Nm;
            private String Client_Name;
            private String Client_Phone;
            private Object Client_Another_Phone;
            private String Client_Address;
            private int Order_CityID;
            private String City_L_Nm;
            private int Order_AreaID;
            private String Area_L_Nm;
            private int DeliveryID;
            private String Polessa_No;
            private String Polessa_Url;
            private double PolessaNo;
            private String Order_Date;
            private String Order_Date_Rep;
            private String Shipping_Date;
            private double Shipping_Dayes;
            private String Delivery_Date;
            private String Delivery_Day_Name;
            private double Ord_Quantity;
            private double Ord_Total_Price;
            private double Ord_Ship_Discount;
            private double Ord_Ship_Price;
            private double Ord_Ship_Client;
            private String Sender_Phone_No;

            public double getOrd_Ship_Discount() {
                return Ord_Ship_Discount;
            }

            public void setOrd_Ship_Discount(double ord_Ship_Discount) {
                Ord_Ship_Discount = ord_Ship_Discount;
            }

            public String getSender_Phone_No() {
                return Sender_Phone_No;
            }

            public void setSender_Phone_No(String sender_Phone_No) {
                Sender_Phone_No = sender_Phone_No;
            }

            public double getOrd_Ship_Client() {
                return Ord_Ship_Client;
            }

            public void setOrd_Ship_Client(double ord_Ship_Client) {
                Ord_Ship_Client = ord_Ship_Client;
            }

            private double Ord_Final_Price;
            private Object Ord_Note;
            private int StatusID;
            private String Stat_App_Nm;
            private String Stat_Client_Nm;
            private String FullName;
            private String Ord_Content;
            private int RecordSerial;
            private String Ord_Doc_No;
            private String Client_Store_Nm;
            private int Payment_Status;
            private String Pay_En_Nm;
            private String Pay_Ar_Nm;
            private double Spear_Ord_Amt;

            public int getOrderUID() {
                return OrderUID;
            }

            public void setOrderUID(int OrderUID) {
                this.OrderUID = OrderUID;
            }

            public int getOrderSheetID() {
                return OrderSheetID;
            }

            public void setOrderSheetID(int OrderSheetID) {
                this.OrderSheetID = OrderSheetID;
            }

            public int getClientID() {
                return ClientID;
            }

            public void setClientID(int ClientID) {
                this.ClientID = ClientID;
            }

            public String getImgUrl() {
                return ImgUrl;
            }

            public void setImgUrl(String ImgUrl) {
                this.ImgUrl = ImgUrl;
            }

            public String getClient_L_Nm() {
                return Client_L_Nm;
            }

            public void setClient_L_Nm(String Client_L_Nm) {
                this.Client_L_Nm = Client_L_Nm;
            }

            public String getClient_F_Nm() {
                return Client_F_Nm;
            }

            public void setClient_F_Nm(String Client_F_Nm) {
                this.Client_F_Nm = Client_F_Nm;
            }

            public String getClient_Name() {
                return Client_Name;
            }

            public void setClient_Name(String Client_Name) {
                this.Client_Name = Client_Name;
            }

            public String getClient_Phone() {
                return Client_Phone;
            }

            public void setClient_Phone(String Client_Phone) {
                this.Client_Phone = Client_Phone;
            }

            public Object getClient_Another_Phone() {
                return Client_Another_Phone;
            }

            public void setClient_Another_Phone(Object Client_Another_Phone) {
                this.Client_Another_Phone = Client_Another_Phone;
            }

            public String getClient_Address() {
                return Client_Address;
            }

            public void setClient_Address(String Client_Address) {
                this.Client_Address = Client_Address;
            }

            public int getOrder_CityID() {
                return Order_CityID;
            }

            public void setOrder_CityID(int Order_CityID) {
                this.Order_CityID = Order_CityID;
            }

            public String getCity_L_Nm() {
                return City_L_Nm;
            }

            public void setCity_L_Nm(String City_L_Nm) {
                this.City_L_Nm = City_L_Nm;
            }

            public int getOrder_AreaID() {
                return Order_AreaID;
            }

            public void setOrder_AreaID(int Order_AreaID) {
                this.Order_AreaID = Order_AreaID;
            }

            public String getArea_L_Nm() {
                return Area_L_Nm;
            }

            public void setArea_L_Nm(String Area_L_Nm) {
                this.Area_L_Nm = Area_L_Nm;
            }

            public int getDeliveryID() {
                return DeliveryID;
            }

            public void setDeliveryID(int DeliveryID) {
                this.DeliveryID = DeliveryID;
            }

            public String getPolessa_No() {
                return Polessa_No;
            }

            public void setPolessa_No(String Polessa_No) {
                this.Polessa_No = Polessa_No;
            }

            public String getPolessa_Url() {
                return Polessa_Url;
            }

            public void setPolessa_Url(String Polessa_Url) {
                this.Polessa_Url = Polessa_Url;
            }

            public double getPolessaNo() {
                return PolessaNo;
            }

            public void setPolessaNo(double PolessaNo) {
                this.PolessaNo = PolessaNo;
            }

            public String getOrder_Date() {
                return Order_Date;
            }

            public void setOrder_Date(String Order_Date) {
                this.Order_Date = Order_Date;
            }

            public String getOrder_Date_Rep() {
                return Order_Date_Rep;
            }

            public void setOrder_Date_Rep(String Order_Date_Rep) {
                this.Order_Date_Rep = Order_Date_Rep;
            }

            public String getShipping_Date() {
                return Shipping_Date;
            }

            public void setShipping_Date(String Shipping_Date) {
                this.Shipping_Date = Shipping_Date;
            }

            public double getShipping_Dayes() {
                return Shipping_Dayes;
            }

            public void setShipping_Dayes(double Shipping_Dayes) {
                this.Shipping_Dayes = Shipping_Dayes;
            }

            public String getDelivery_Date() {
                return Delivery_Date;
            }

            public void setDelivery_Date(String Delivery_Date) {
                this.Delivery_Date = Delivery_Date;
            }

            public String getDelivery_Day_Name() {
                return Delivery_Day_Name;
            }

            public void setDelivery_Day_Name(String Delivery_Day_Name) {
                this.Delivery_Day_Name = Delivery_Day_Name;
            }

            public double getOrd_Quantity() {
                return Ord_Quantity;
            }

            public void setOrd_Quantity(double Ord_Quantity) {
                this.Ord_Quantity = Ord_Quantity;
            }

            public double getOrd_Total_Price() {
                return Ord_Total_Price;
            }

            public void setOrd_Total_Price(double Ord_Total_Price) {
                this.Ord_Total_Price = Ord_Total_Price;
            }

            public double getOrd_Ship_Price() {
                return Ord_Ship_Price;
            }

            public void setOrd_Ship_Price(double Ord_Ship_Price) {
                this.Ord_Ship_Price = Ord_Ship_Price;
            }

            public double getOrd_Final_Price() {
                return Ord_Final_Price;
            }

            public void setOrd_Final_Price(double Ord_Final_Price) {
                this.Ord_Final_Price = Ord_Final_Price;
            }

            public Object getOrd_Note() {
                return Ord_Note;
            }

            public void setOrd_Note(Object Ord_Note) {
                this.Ord_Note = Ord_Note;
            }

            public int getStatusID() {
                return StatusID;
            }

            public void setStatusID(int StatusID) {
                this.StatusID = StatusID;
            }

            public String getStat_App_Nm() {
                return Stat_App_Nm;
            }

            public void setStat_App_Nm(String Stat_App_Nm) {
                this.Stat_App_Nm = Stat_App_Nm;
            }

            public String getStat_Client_Nm() {
                return Stat_Client_Nm;
            }

            public void setStat_Client_Nm(String Stat_Client_Nm) {
                this.Stat_Client_Nm = Stat_Client_Nm;
            }

            public String getFullName() {
                return FullName;
            }

            public void setFullName(String FullName) {
                this.FullName = FullName;
            }

            public String getOrd_Content() {
                return Ord_Content;
            }

            public void setOrd_Content(String Ord_Content) {
                this.Ord_Content = Ord_Content;
            }

            public int getRecordSerial() {
                return RecordSerial;
            }

            public void setRecordSerial(int RecordSerial) {
                this.RecordSerial = RecordSerial;
            }

            public String getOrd_Doc_No() {
                return Ord_Doc_No;
            }

            public void setOrd_Doc_No(String Ord_Doc_No) {
                this.Ord_Doc_No = Ord_Doc_No;
            }

            public String getClient_Store_Nm() {
                return Client_Store_Nm;
            }

            public void setClient_Store_Nm(String Client_Store_Nm) {
                this.Client_Store_Nm = Client_Store_Nm;
            }

            public int getPayment_Status() {
                return Payment_Status;
            }

            public void setPayment_Status(int Payment_Status) {
                this.Payment_Status = Payment_Status;
            }

            public String getPay_En_Nm() {
                return Pay_En_Nm;
            }

            public void setPay_En_Nm(String Pay_En_Nm) {
                this.Pay_En_Nm = Pay_En_Nm;
            }

            public String getPay_Ar_Nm() {
                return Pay_Ar_Nm;
            }

            public void setPay_Ar_Nm(String Pay_Ar_Nm) {
                this.Pay_Ar_Nm = Pay_Ar_Nm;
            }

            public double getSpear_Ord_Amt() {
                return Spear_Ord_Amt;
            }

            public void setSpear_Ord_Amt(double Spear_Ord_Amt) {
                this.Spear_Ord_Amt = Spear_Ord_Amt;
            }
        }
    }
}