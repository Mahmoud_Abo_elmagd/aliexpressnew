package com.example.zone.dataModel;

import com.google.gson.annotations.SerializedName;

public class OrderCountModel {

    @SerializedName("result")
    private ResultBean result;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        @SerializedName("user")
        private UserBean user;

        public UserBean getUser() {
            return user;
        }

        public void setUser(UserBean user) {
            this.user = user;
        }

        public static class UserBean {
            @SerializedName("Dash_EmployeeID")
            private int DashEmployeeID;
            @SerializedName("Dash_allorders")
            private int DashAllorders;
            @SerializedName("Dash_inshipping")
            private int DashInshipping;
            @SerializedName("Dash_delevired")
            private int DashDelevired;
            @SerializedName("Dash_postponed")
            private int DashPostponed;
            @SerializedName("Dash_canceled")
            private int DashCanceled;
            @SerializedName("Dash_recivereq")
            private int DashRecivereq;
            @SerializedName("Dash_partdelevired")
            private int DashPartdelevired;
            @SerializedName("Dash_returned")
            private int DashReturned;
            @SerializedName("Dash_empdelivery")
            private int DashEmpdelivery;
            @SerializedName("Dash_empbalance")
            private double DashEmpbalance;
            @SerializedName("Dash_empcollected")
            private double DashEmpcollected;
            @SerializedName("Dash_empnotcollected")
            private double DashEmpnotcollected;

            public int getDashEmployeeID() {
                return DashEmployeeID;
            }

            public void setDashEmployeeID(int DashEmployeeID) {
                this.DashEmployeeID = DashEmployeeID;
            }

            public int getDashAllorders() {
                return DashAllorders;
            }

            public void setDashAllorders(int DashAllorders) {
                this.DashAllorders = DashAllorders;
            }

            public int getDashInshipping() {
                return DashInshipping;
            }

            public void setDashInshipping(int DashInshipping) {
                this.DashInshipping = DashInshipping;
            }

            public int getDashDelevired() {
                return DashDelevired;
            }

            public void setDashDelevired(int DashDelevired) {
                this.DashDelevired = DashDelevired;
            }

            public int getDashPostponed() {
                return DashPostponed;
            }

            public void setDashPostponed(int DashPostponed) {
                this.DashPostponed = DashPostponed;
            }

            public int getDashCanceled() {
                return DashCanceled;
            }

            public void setDashCanceled(int DashCanceled) {
                this.DashCanceled = DashCanceled;
            }

            public int getDashRecivereq() {
                return DashRecivereq;
            }

            public void setDashRecivereq(int DashRecivereq) {
                this.DashRecivereq = DashRecivereq;
            }

            public int getDashPartdelevired() {
                return DashPartdelevired;
            }

            public void setDashPartdelevired(int DashPartdelevired) {
                this.DashPartdelevired = DashPartdelevired;
            }

            public int getDashReturned() {
                return DashReturned;
            }

            public void setDashReturned(int DashReturned) {
                this.DashReturned = DashReturned;
            }

            public int getDashEmpdelivery() {
                return DashEmpdelivery;
            }

            public void setDashEmpdelivery(int DashEmpdelivery) {
                this.DashEmpdelivery = DashEmpdelivery;
            }

            public double getDashEmpbalance() {
                return DashEmpbalance;
            }

            public void setDashEmpbalance(double DashEmpbalance) {
                this.DashEmpbalance = DashEmpbalance;
            }

            public double getDashEmpcollected() {
                return DashEmpcollected;
            }

            public void setDashEmpcollected(int DashEmpcollected) {
                this.DashEmpcollected = DashEmpcollected;
            }

            public double getDashEmpnotcollected() {
                return DashEmpnotcollected;
            }

            public void setDashEmpnotcollected(int DashEmpnotcollected) {
                this.DashEmpnotcollected = DashEmpnotcollected;
            }
        }
    }
}