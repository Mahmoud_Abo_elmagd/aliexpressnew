package com.example.zone.dataModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationModel {

    @SerializedName("result")
    private ResultBean result;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        @SerializedName("user")
        private List<UserBean> user;

        public List<UserBean> getUser() {
            return user;
        }

        public void setUser(List<UserBean> user) {
            this.user = user;
        }

        public static class UserBean {
            @SerializedName("NoticUID")
            private int NoticUID;
            @SerializedName("Employee_ID")
            private int EmployeeID;
            @SerializedName("UserUID")
            private int UserUID;
            @SerializedName("User_ID")
            private int UserID;
            @SerializedName("FullName")
            private String FullName;
            @SerializedName("Notic_Title")
            private String NoticTitle;
            @SerializedName("Notic_Mssge")
            private String NoticMssge;
            @SerializedName("Notic_Date")
            private String NoticDate;
            @SerializedName("Notic_Time")
            private String NoticTime;

            public int getNoticUID() {
                return NoticUID;
            }

            public void setNoticUID(int NoticUID) {
                this.NoticUID = NoticUID;
            }

            public int getEmployeeID() {
                return EmployeeID;
            }

            public void setEmployeeID(int EmployeeID) {
                this.EmployeeID = EmployeeID;
            }

            public int getUserUID() {
                return UserUID;
            }

            public void setUserUID(int UserUID) {
                this.UserUID = UserUID;
            }

            public int getUserID() {
                return UserID;
            }

            public void setUserID(int UserID) {
                this.UserID = UserID;
            }

            public String getFullName() {
                return FullName;
            }

            public void setFullName(String FullName) {
                this.FullName = FullName;
            }

            public String getNoticTitle() {
                return NoticTitle;
            }

            public void setNoticTitle(String NoticTitle) {
                this.NoticTitle = NoticTitle;
            }

            public String getNoticMssge() {
                return NoticMssge;
            }

            public void setNoticMssge(String NoticMssge) {
                this.NoticMssge = NoticMssge;
            }

            public String getNoticDate() {
                return NoticDate;
            }

            public void setNoticDate(String NoticDate) {
                this.NoticDate = NoticDate;
            }

            public String getNoticTime() {
                return NoticTime;
            }

            public void setNoticTime(String NoticTime) {
                this.NoticTime = NoticTime;
            }
        }
    }
}