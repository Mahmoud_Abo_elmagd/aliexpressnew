package com.example.zone.dataModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReceiveModel {

    @SerializedName("result")
    private ResultBean result;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        @SerializedName("user")
        private List<UserBean> user;

        public List<UserBean> getUser() {
            return user;
        }

        public void setUser(List<UserBean> user) {
            this.user = user;
        }

        public static class UserBean {
            @SerializedName("ReciveRequestUID")
            private int ReciveRequestUID;
            @SerializedName("ClientID")
            private int ClientID;
            @SerializedName("Client_L_Nm")
            private String ClientLNm;
            @SerializedName("Client_F_Nm")
            private String ClientFNm;
            @SerializedName("Phone_No")
            private String PhoneNo;
            @SerializedName("Client_Store_Nm")
            private String ClientStoreNm;
            @SerializedName("Client_AccDebtor")
            private int ClientAccDebtor;
            @SerializedName("Client_AccCreditor")
            private int ClientAccCreditor;
            @SerializedName("Client_AccBalance")
            private int ClientAccBalance;
            @SerializedName("Client_Address")
            private String ClientAddress;
            @SerializedName("ImgUrl")
            private String ImgUrl;
            @SerializedName("FullName")
            private String FullName;
            @SerializedName("Employee_Phone")
            private String EmployeePhone;
            @SerializedName("Client_Address_ID")
            private int ClientAddressID;
            @SerializedName("EmployeeID")
            private int EmployeeID;
            @SerializedName("Recive_Note")
            private String ReciveNote;
            @SerializedName("Recive_Date")
            private String ReciveDate;
            @SerializedName("Recive_Date_Rep")
            private String ReciveDateRep;
            @SerializedName("Orders_No")
            private int OrdersNo;
            @SerializedName("Req_Visit_Amt")
            private int ReqVisitAmt;
            @SerializedName("ReqRecive_ID")
            private int ReqReciveID;
            @SerializedName("ReqPayment_ID")
            private int ReqPaymentID;
            @SerializedName("Branch_ID")
            private int BranchID;
            @SerializedName("ReqPayment_Ar_Nm")
            private String ReqPaymentArNm;
            @SerializedName("ReqRecive_Ar_Nm")
            private String ReqReciveArNm;
            @SerializedName("AdUser")
            private String AdUser;
            @SerializedName("Price_List_ID")
            private int PriceListID;
            @SerializedName("Area_ID")
            private int AreaID;
            @SerializedName("ShippingAmt")
            private int ShippingAmt;
            @SerializedName("Req_Amt_Duo")
            private int ReqAmtDuo;

            public int getReciveRequestUID() {
                return ReciveRequestUID;
            }

            public void setReciveRequestUID(int ReciveRequestUID) {
                this.ReciveRequestUID = ReciveRequestUID;
            }

            public int getClientID() {
                return ClientID;
            }

            public void setClientID(int ClientID) {
                this.ClientID = ClientID;
            }

            public String getClientLNm() {
                return ClientLNm;
            }

            public void setClientLNm(String ClientLNm) {
                this.ClientLNm = ClientLNm;
            }

            public String getClientFNm() {
                return ClientFNm;
            }

            public void setClientFNm(String ClientFNm) {
                this.ClientFNm = ClientFNm;
            }

            public String getPhoneNo() {
                return PhoneNo;
            }

            public void setPhoneNo(String PhoneNo) {
                this.PhoneNo = PhoneNo;
            }

            public String getClientStoreNm() {
                return ClientStoreNm;
            }

            public void setClientStoreNm(String ClientStoreNm) {
                this.ClientStoreNm = ClientStoreNm;
            }

            public int getClientAccDebtor() {
                return ClientAccDebtor;
            }

            public void setClientAccDebtor(int ClientAccDebtor) {
                this.ClientAccDebtor = ClientAccDebtor;
            }

            public int getClientAccCreditor() {
                return ClientAccCreditor;
            }

            public void setClientAccCreditor(int ClientAccCreditor) {
                this.ClientAccCreditor = ClientAccCreditor;
            }

            public int getClientAccBalance() {
                return ClientAccBalance;
            }

            public void setClientAccBalance(int ClientAccBalance) {
                this.ClientAccBalance = ClientAccBalance;
            }

            public String getClientAddress() {
                return ClientAddress;
            }

            public void setClientAddress(String ClientAddress) {
                this.ClientAddress = ClientAddress;
            }

            public String getImgUrl() {
                return ImgUrl;
            }

            public void setImgUrl(String ImgUrl) {
                this.ImgUrl = ImgUrl;
            }

            public String getFullName() {
                return FullName;
            }

            public void setFullName(String FullName) {
                this.FullName = FullName;
            }

            public String getEmployeePhone() {
                return EmployeePhone;
            }

            public void setEmployeePhone(String EmployeePhone) {
                this.EmployeePhone = EmployeePhone;
            }

            public int getClientAddressID() {
                return ClientAddressID;
            }

            public void setClientAddressID(int ClientAddressID) {
                this.ClientAddressID = ClientAddressID;
            }

            public int getEmployeeID() {
                return EmployeeID;
            }

            public void setEmployeeID(int EmployeeID) {
                this.EmployeeID = EmployeeID;
            }

            public String getReciveNote() {
                return ReciveNote;
            }

            public void setReciveNote(String ReciveNote) {
                this.ReciveNote = ReciveNote;
            }

            public String getReciveDate() {
                return ReciveDate;
            }

            public void setReciveDate(String ReciveDate) {
                this.ReciveDate = ReciveDate;
            }

            public String getReciveDateRep() {
                return ReciveDateRep;
            }

            public void setReciveDateRep(String ReciveDateRep) {
                this.ReciveDateRep = ReciveDateRep;
            }

            public int getOrdersNo() {
                return OrdersNo;
            }

            public void setOrdersNo(int OrdersNo) {
                this.OrdersNo = OrdersNo;
            }

            public int getReqVisitAmt() {
                return ReqVisitAmt;
            }

            public void setReqVisitAmt(int ReqVisitAmt) {
                this.ReqVisitAmt = ReqVisitAmt;
            }

            public int getReqReciveID() {
                return ReqReciveID;
            }

            public void setReqReciveID(int ReqReciveID) {
                this.ReqReciveID = ReqReciveID;
            }

            public int getReqPaymentID() {
                return ReqPaymentID;
            }

            public void setReqPaymentID(int ReqPaymentID) {
                this.ReqPaymentID = ReqPaymentID;
            }

            public int getBranchID() {
                return BranchID;
            }

            public void setBranchID(int BranchID) {
                this.BranchID = BranchID;
            }

            public String getReqPaymentArNm() {
                return ReqPaymentArNm;
            }

            public void setReqPaymentArNm(String ReqPaymentArNm) {
                this.ReqPaymentArNm = ReqPaymentArNm;
            }

            public String getReqReciveArNm() {
                return ReqReciveArNm;
            }

            public void setReqReciveArNm(String ReqReciveArNm) {
                this.ReqReciveArNm = ReqReciveArNm;
            }

            public String getAdUser() {
                return AdUser;
            }

            public void setAdUser(String AdUser) {
                this.AdUser = AdUser;
            }

            public int getPriceListID() {
                return PriceListID;
            }

            public void setPriceListID(int PriceListID) {
                this.PriceListID = PriceListID;
            }

            public int getAreaID() {
                return AreaID;
            }

            public void setAreaID(int AreaID) {
                this.AreaID = AreaID;
            }

            public int getShippingAmt() {
                return ShippingAmt;
            }

            public void setShippingAmt(int ShippingAmt) {
                this.ShippingAmt = ShippingAmt;
            }

            public int getReqAmtDuo() {
                return ReqAmtDuo;
            }

            public void setReqAmtDuo(int ReqAmtDuo) {
                this.ReqAmtDuo = ReqAmtDuo;
            }
        }
    }
}