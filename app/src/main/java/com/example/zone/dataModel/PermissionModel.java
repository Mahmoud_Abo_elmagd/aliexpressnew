package com.example.zone.dataModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PermissionModel {

    @SerializedName("result")
    private ResultBean result;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        @SerializedName("user")
        private List<UserBean> user;

        public List<UserBean> getUser() {
            return user;
        }

        public void setUser(List<UserBean> user) {
            this.user = user;
        }

        public static class UserBean {
            @SerializedName("PermUID")
            private int PermUID;
            @SerializedName("Role_ID")
            private int RoleID;
            @SerializedName("Screen_ID")
            private int ScreenID;
            @SerializedName("Perm_Status")
            private boolean PermStatus;
            @SerializedName("Screen_Name")
            private String ScreenName;

            public int getPermUID() {
                return PermUID;
            }

            public void setPermUID(int PermUID) {
                this.PermUID = PermUID;
            }

            public int getRoleID() {
                return RoleID;
            }

            public void setRoleID(int RoleID) {
                this.RoleID = RoleID;
            }

            public int getScreenID() {
                return ScreenID;
            }

            public void setScreenID(int ScreenID) {
                this.ScreenID = ScreenID;
            }

            public boolean isPermStatus() {
                return PermStatus;
            }

            public void setPermStatus(boolean PermStatus) {
                this.PermStatus = PermStatus;
            }

            public String getScreenName() {
                return ScreenName;
            }

            public void setScreenName(String ScreenName) {
                this.ScreenName = ScreenName;
            }
        }
    }
}
