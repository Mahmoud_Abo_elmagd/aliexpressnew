package com.example.zone.dataModel;

import com.google.gson.annotations.SerializedName;

public class UserModel {

    @SerializedName("result")
    private ResultBean result;

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        @SerializedName("user")
        private UserBean user;

        public UserBean getUser() {
            return user;
        }

        public void setUser(UserBean user) {
            this.user = user;
        }

        public static class UserBean {
            @SerializedName("UserUID")
            private int UserUID;
            @SerializedName("RoleID")
            private int RoleID;
            @SerializedName("EmployeeID")
            private int EmployeeID;
            @SerializedName("ImgUrl")
            private String ImgUrl;
            @SerializedName("FullName")
            private String FullName;
            @SerializedName("User_Name")
            private String UserName;
            @SerializedName("User_State")
            private boolean UserState;
            @SerializedName("Job_L_Nm")
            private String JobLNm;
            @SerializedName("Is_OutDoor")
            private boolean IsOutDoor;
            @SerializedName("Role_Ar_Nm")
            private String RoleArNm;
            @SerializedName("User_Fcm")
            private Object UserFcm;
            @SerializedName("User_Lat")
            private Object UserLat;
            @SerializedName("User_Lng")
            private Object UserLng;
            @SerializedName("RollID")
            private Object RollID;
            @SerializedName("Branch_ID")
            private int BranchID;
            @SerializedName("Branch_Ar_Nm")
            private String BranchArNm;
            @SerializedName("StoreUID")
            private int StoreUID;
            @SerializedName("Store_Ar_Nm")
            private String StoreArNm;

            public int getUserUID() {
                return UserUID;
            }

            public void setUserUID(int UserUID) {
                this.UserUID = UserUID;
            }

            public int getRoleID() {
                return RoleID;
            }

            public void setRoleID(int RoleID) {
                this.RoleID = RoleID;
            }

            public int getEmployeeID() {
                return EmployeeID;
            }

            public void setEmployeeID(int EmployeeID) {
                this.EmployeeID = EmployeeID;
            }

            public String getImgUrl() {
                return ImgUrl;
            }

            public void setImgUrl(String ImgUrl) {
                this.ImgUrl = ImgUrl;
            }

            public String getFullName() {
                return FullName;
            }

            public void setFullName(String FullName) {
                this.FullName = FullName;
            }

            public String getUserName() {
                return UserName;
            }

            public void setUserName(String UserName) {
                this.UserName = UserName;
            }

            public boolean isUserState() {
                return UserState;
            }

            public void setUserState(boolean UserState) {
                this.UserState = UserState;
            }

            public String getJobLNm() {
                return JobLNm;
            }

            public void setJobLNm(String JobLNm) {
                this.JobLNm = JobLNm;
            }

            public boolean isIsOutDoor() {
                return IsOutDoor;
            }

            public void setIsOutDoor(boolean IsOutDoor) {
                this.IsOutDoor = IsOutDoor;
            }

            public String getRoleArNm() {
                return RoleArNm;
            }

            public void setRoleArNm(String RoleArNm) {
                this.RoleArNm = RoleArNm;
            }

            public Object getUserFcm() {
                return UserFcm;
            }

            public void setUserFcm(Object UserFcm) {
                this.UserFcm = UserFcm;
            }

            public Object getUserLat() {
                return UserLat;
            }

            public void setUserLat(Object UserLat) {
                this.UserLat = UserLat;
            }

            public Object getUserLng() {
                return UserLng;
            }

            public void setUserLng(Object UserLng) {
                this.UserLng = UserLng;
            }

            public Object getRollID() {
                return RollID;
            }

            public void setRollID(Object RollID) {
                this.RollID = RollID;
            }

            public int getBranchID() {
                return BranchID;
            }

            public void setBranchID(int BranchID) {
                this.BranchID = BranchID;
            }

            public String getBranchArNm() {
                return BranchArNm;
            }

            public void setBranchArNm(String BranchArNm) {
                this.BranchArNm = BranchArNm;
            }

            public int getStoreUID() {
                return StoreUID;
            }

            public void setStoreUID(int StoreUID) {
                this.StoreUID = StoreUID;
            }

            public String getStoreArNm() {
                return StoreArNm;
            }

            public void setStoreArNm(String StoreArNm) {
                this.StoreArNm = StoreArNm;
            }
        }
    }
}