package com.example.zone.Utilities;

import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.view.GravityCompat;
import androidx.databinding.BindingAdapter;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.zone.R;
import com.google.android.material.tabs.TabLayout;


/**
 * Created by MAHMOUD on 28/8/2020.
 */

public class BindingAdapters {


    @BindingAdapter(value = {"bind:imageUrl", "bind:defaultImage"}, requireAll = false)
    public static void loadImage(ImageView imageView, String url, Integer defaultImage) {
        if (defaultImage == null) defaultImage = -1;
        Utilities.downLoadImage(imageView, url, defaultImage);
    }

    @BindingAdapter(value = {"bind:imageUrlDefault", "bind:defaultImage"}, requireAll = false)
    public static void loadImageDefault(ImageView imageView, String url, Integer defaultImage) {
        defaultImage = R.drawable.ic_launcher_background;
        Utilities.downLoadImage(imageView, url, defaultImage);
    }

    @BindingAdapter("bind:imageBitmap")
    public static void loadImage(ImageView iv, Bitmap bitmap) {
        iv.setImageBitmap(bitmap);
    }

    @BindingAdapter("bind:imageSrc")
    public static void loadImage(ImageView iv, Drawable drawable) {
        iv.setImageDrawable(drawable);
    }

    @BindingAdapter("bind:imageResource")
    public static void loadImage(ImageView iv, int drawable) {
        iv.setImageResource(drawable);
    }

    @BindingAdapter("bind:draw")
    public static void opendrawable(DrawerLayout drawerLayout, String url) {
        if (url.equals("0")) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else if (url.equals("1")) {
            drawerLayout.openDrawer(GravityCompat.START);
        }
    }

    @BindingAdapter("bind:backgroundResource")
    public static void backgroundResource(View view, int resource) {
        view.setBackgroundResource(resource);
    }

    @BindingAdapter("bind:backgroundTintResource")
    public static void backgroundTintResource(View view, int resource) {
        view.setBackgroundColor(resource);
    }

    @BindingAdapter({"setUpWithViewpager"})
    public static void setUpWithViewpager(final TabLayout tabLayout, ViewPager viewPager) {
        viewPager.addOnAdapterChangeListener(new ViewPager.OnAdapterChangeListener() {
            @Override
            public void onAdapterChanged(@NonNull ViewPager viewPager,
                                         @Nullable PagerAdapter oldAdapter, @Nullable PagerAdapter newAdapter) {
                if (oldAdapter == null && newAdapter == null) {
                    return;
                }
                Log.i("TAG", "onAdapterChanged");
                tabLayout.setupWithViewPager(viewPager);
            }
        });
    }

    @BindingAdapter("button:animate")
    public static void animateButton(final Button button, final boolean animate) {
        AnimationDrawable animationDrawable = (AnimationDrawable) button.getBackground();
        if (animate) {
            button.setEnabled(false);
            animationDrawable.start();
        } else {
            animationDrawable.stop();
            button.setEnabled(true);
        }
    }

    @BindingAdapter("bind:topBottomOf")
    public static void topBottomOf(View view, int id) {
        ConstraintLayout constraintLayout = (ConstraintLayout) view.getParent();

        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(constraintLayout);

        constraintSet.connect(view.getId(), ConstraintSet.TOP, id, ConstraintSet.BOTTOM);
        constraintSet.applyTo(constraintLayout);
    }

    @BindingAdapter({"video"})
    public static void bindVideo(final VideoView videoView, String url) {
        if (!url.equals("")) {
            videoView.setVideoURI(Uri.parse(url));
            videoView.seekTo(200);
        }
    }

    @BindingAdapter({"drawer:state"})
    public static void drawerState(final DrawerLayout drawerLayout, boolean open) {
        if (open) {
            drawerLayout.openDrawer(GravityCompat.START);
        } else {
            if (drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    @BindingAdapter({"recycler:adapter"})
    public static void adapter(final RecyclerView recyclerView, RecyclerView.Adapter adapter) {
        recyclerView.setAdapter(adapter);
    }

    @BindingAdapter({"textview:paint"})
    public static void textPaint(TextView textView, int paint) {
        textView.setPaintFlags(textView.getPaintFlags() | paint);
    }

    @BindingAdapter({"item:decoration"})
    public static void itemDecoration(final RecyclerView recyclerView, RecyclerView.ItemDecoration itemDecoration) {
        recyclerView.addItemDecoration(itemDecoration);
    }
}
