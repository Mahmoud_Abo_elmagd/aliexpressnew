package com.example.zone.Utilities;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.Toolbar;

import com.squareup.picasso.Picasso;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;


/**
 * Created by MAHMOUD on 28/8/2020.
 */
public class Utilities {
    public static final int LOCATION_REQUEST_PERMISSION = 6518;
    public static Activity activity;

    public static boolean checkNetworkConnectivity(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public static void noInternet(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Internet connection");
        builder.setMessage("Connect to a network");

        builder.setPositiveButton("OK", null);
        builder.show();
    }

    public static String pad(long date) {
        if (date < 9)
            return "0" + date;
        return date + "";
    }

    public static void downLoadImage(ImageView imageView, String url, int drawable) {
        if (url == null || url.trim().isEmpty()) url = "invalid";
        try {
            if (url != null && !url.equals("") && !url.equals("null")) {
                if (drawable != -1)
                    Picasso.get()
                            .load(url)
                            .placeholder(drawable)
                            .into(imageView);
                else
                    Picasso.get()
                            .load(url)
                            .into(imageView);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void downLoadImage(ImageView imageView, String url) {
        downLoadImage(imageView, url, -1);
    }

    public static void setViewData(LinearLayout dataLayout, String title, String data) {
       /* TextView titleTxv = dataLayout.findViewById(R.id.title_txv);
        TextView dataTxv = dataLayout.findViewById(R.id.data_txv);
        titleTxv.setText(title);
        dataTxv.setText(data);*/
    }


    public static void setLang(Activity activity, String lang) {

        Resources res = activity.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.setLocale(new Locale(lang));

        res.updateConfiguration(conf, dm);
    }

    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static void setToolbar(final Activity activity, Toolbar toolbar, String title, boolean isElevated) {
        if (isElevated && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(GlobalVariables.TOOLBAR_ELEVATION);
        }

        /*toolbar.findViewById(R.id.back_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onBackPressed();
            }
        });

        TextView toolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText(title);*/
    }

    public static boolean checkDateTime(Activity activity, String date, String time) {
        Calendar calendar = Calendar.getInstance();
        String current = calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.DAY_OF_MONTH)
                + " " + calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE) + ":" + calendar.get(Calendar.MILLISECOND);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date currentDate = simpleDateFormat.parse(current);
            Date selectedDate = simpleDateFormat.parse(date + " " + time);

            if (selectedDate.before(currentDate))
                return false;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static String padNumber(int number) {
        String n = number + "";
        if (number <= 9)
            n = "0" + number;
        return n;
    }

    public static long getDateDifference(Context context, String created) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm aa", Locale.ENGLISH);
        try {

            Date currentDate = simpleDateFormat.parse(simpleDateFormat.format(new Date()));
            Date createdDate = simpleDateFormat.parse(created);
            Log.e("createdDate", createdDate.toString());
            Log.e("currentDate", currentDate.toString());
            long difference = (currentDate.getTime() - createdDate.getTime());
            return difference;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }


    public static String formatTimeHours(String time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm");
        try {
            Date date = simpleDateFormat.parse(time);
            simpleDateFormat = new SimpleDateFormat("hh:mm aa");
            time = simpleDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time;
    }


    public static int convertDPtoInt(Activity activity, int padding_in_dp) {
        final float scale = activity.getResources().getDisplayMetrics().density;
        return (int) (padding_in_dp * scale + 0.5f);
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static void hideKeyboard(Activity activity) {
        try {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            //Find the currently focused view, so we can grab the correct window token from it.
            View view = activity.getCurrentFocus();
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = new View(activity);
            }
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void hideKeyboard(Activity activity, View view) {
        try {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static long getDiffBetweenTime(String current, String other) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
            Date currentDate = simpleDateFormat.parse(current);
            Date otherDate = simpleDateFormat.parse(other);
            return (otherDate.getTime() - currentDate.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static long getDiffBetweenTime(String date) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
            Date currentDate = new Date();
            Date otherDate = simpleDateFormat.parse(date);
            return (otherDate.getTime() - currentDate.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String formatTimeToDate(String time) {
        String date = time.substring(0, 10);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
        try {
            Date timeDate = simpleDateFormat.parse(time);
            simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
            date = simpleDateFormat.format(timeDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = DateFormat.format("dd-MM-yyyy", cal).toString();
        return date;
    }

    public static Bitmap getCircularBitmap(Bitmap bitmap) {
        Bitmap output;

        if (bitmap.getWidth() > bitmap.getHeight()) {
            output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        } else {
            output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        float r = 0;

        if (bitmap.getWidth() > bitmap.getHeight()) {
            r = bitmap.getHeight() / 2;
        } else {
            r = bitmap.getWidth() / 2;
        }

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(r, r, r, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }

    public static Bitmap drawBorderCircularBitmap(Bitmap bitmap) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int radius = Math.min(h / 2, w / 2);
        Bitmap output = Bitmap.createBitmap(w + 8, h + 8, Bitmap.Config.ARGB_8888);

        Paint p = new Paint();
        p.setAntiAlias(true);

        Canvas c = new Canvas(output);
        c.drawARGB(0, 0, 0, 0);
        p.setStyle(Paint.Style.FILL);

        c.drawCircle((w / 2) + 4, (h / 2) + 4, radius, p);

        p.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));

        c.drawBitmap(bitmap, 4, 4, p);
        p.setXfermode(null);
        p.setStyle(Paint.Style.STROKE);
        p.setColor(Color.parseColor("#02aee7"));
        p.setStrokeWidth(3);
        c.drawCircle((w / 2) + 4, (h / 2) + 4, radius, p);

        return output;
    }

    public static boolean isToday(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
        String today = dateFormat.format(new Date());
        return date.equals(today);
    }

    public static Activity getActivity() throws ClassNotFoundException, NoSuchMethodException, NoSuchFieldException, InvocationTargetException, IllegalAccessException {
        Class activityThreadClass = Class.forName("android.app.ActivityThread");
        Object activityThread = activityThreadClass.getMethod("currentActivityThread").invoke(null);
        Field activitiesField = activityThreadClass.getDeclaredField("mActivities");
        activitiesField.setAccessible(true);

        Map<Object, Object> activities = (Map<Object, Object>) activitiesField.get(activityThread);
        if (activities == null)
            return null;

        for (Object activityRecord : activities.values()) {
            Class activityRecordClass = activityRecord.getClass();
            Field pausedField = activityRecordClass.getDeclaredField("paused");
            pausedField.setAccessible(true);
            if (!pausedField.getBoolean(activityRecord)) {
                Field activityField = activityRecordClass.getDeclaredField("activity");
                activityField.setAccessible(true);
                Activity activity = (Activity) activityField.get(activityRecord);
                return activity;
            }
        }

        return null;
    }

    public static String roundPrice(String price) {
        if (price == null || price.isEmpty()) return "";
        return roundPrice(Double.valueOf(price));
    }

    public static String roundPrice(double price) {
        NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.ENGLISH);
        String pattern = ((DecimalFormat) nf).toPattern();
        String newPattern = pattern.replace("\u00A4", "").trim();
        NumberFormat newFormat = new DecimalFormat(newPattern);
        return newFormat.format(price);
    }

    public static String roundRate(String rate) {
        return roundPrice(Double.valueOf(rate));
    }

    public static String roundRate(double rate) {
        return String.format(Locale.ENGLISH, "%.1f", rate);
    }

    public static String formatOrderDate(String orderDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
        try {
            Date date = simpleDateFormat.parse(orderDate);
            SimpleDateFormat dayname = new SimpleDateFormat("EEEE", Locale.getDefault());
            SimpleDateFormat day = new SimpleDateFormat("d", Locale.ENGLISH);
            SimpleDateFormat month = new SimpleDateFormat("mm", Locale.ENGLISH);
            SimpleDateFormat year = new SimpleDateFormat("yyyy", Locale.ENGLISH);
            orderDate = year.format(date) + "/" + month.format(date) + "/" + day.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return orderDate;
    }

    public static String formatTimestampToDate(String timestamp) {
        return formatTimestampToDate(Long.valueOf(timestamp));
    }

    public static String formatTimestampToDate(long timestamp) {
        String createdAt;
        SimpleDateFormat simpleDateFormat;
        Date date = new Date(timestamp * 1000L);
        if (DateUtils.isToday(date.getTime())) {
            simpleDateFormat = new SimpleDateFormat("h:mm aa", Locale.ENGLISH);
            createdAt = simpleDateFormat.format(date);
        } else {
            simpleDateFormat = new SimpleDateFormat("d/M/yyyy", Locale.ENGLISH);
            createdAt = simpleDateFormat.format(date);
        }
        return createdAt;
    }

    public static String formatTimestampToDateOnly(String timestamp) {
        String createdAt;
        Long timeStamp = Long.valueOf(timestamp);
        Date date = new Date(timeStamp * 1000L);
        SimpleDateFormat day = new SimpleDateFormat("d", Locale.ENGLISH);
        SimpleDateFormat month = new SimpleDateFormat("MMM", Locale.ENGLISH);
        SimpleDateFormat year = new SimpleDateFormat("yyyy", Locale.ENGLISH);
        createdAt = day.format(date) + " " + month.format(date) + "," + year.format(date);
        return createdAt;
    }

    public static void changeStatusBarColor(Activity myActivity, int color, boolean isChange) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Window window = myActivity.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);

            View decor = myActivity.getWindow().getDecorView();
            if (isChange) {
                decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else {
                decor.setSystemUiVisibility(0);
            }
        }
    }


    public static double distance(double lat1, double lat2, double lon1,
                                  double lon2, double el1, double el2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }

    public static Long getTimeStamp() {
        Long timeStamp = System.currentTimeMillis() / 1000;
        return timeStamp;
    }

    public static void sendEmailToUser(Activity activity, String email, String productName) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        i.putExtra(Intent.EXTRA_SUBJECT, "Product Added To Favourite");
        i.putExtra(Intent.EXTRA_TEXT, productName + " Added To Favourite you can open Online Store to see all Special ");
        try {
            activity.startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(activity, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    public static void toastyError(Activity activity, String message) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    public static void toastyError(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void toastyRequiredFieldCustom(Activity activity, String message) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    public static void toastySuccess(Activity activity, String message) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }
}
