package com.example.zone.Utilities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;

import android.graphics.Color;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


/**
 * <h1>Implement reusable dialogs</h1>
 * Dialogs class for all dialogs and toasts
 * <p>
 *
 * @author kemo94
 * @version 1.0
 * @since 2017-08-9
 */

public abstract class Dialogs {


    public static Dialog noInternetDialog;



    public static void showLoading(Activity activity, LoadingDialog loadingDialog) {
        try {
            loadingDialog.show(((AppCompatActivity) activity).getSupportFragmentManager(), "showLoading");

        } catch (Exception e) {

        }
    }


    public static void showToastColor(Context context, String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        View view = toast.getView();
        view.setBackgroundColor(Color.parseColor("#000000"));
        v.setTextColor(Color.WHITE);
        v.setPadding(10, 10, 10, 10);
        toast.show();

    }


    public static void dismissLoading(LoadingDialog loadingDialog) {
        try {
            if (loadingDialog != null)
                loadingDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void showToast(String message, Activity activity) {
        if (message != null && !message.equals(""))
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
    }

    public static void showToast(String message, Context context) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }


    public interface DialogListener {
        void onChoose(int pos);
    }
}
