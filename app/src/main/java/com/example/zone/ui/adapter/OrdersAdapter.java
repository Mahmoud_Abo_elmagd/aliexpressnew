package com.example.zone.ui.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.zone.R;
import com.example.zone.dataModel.OrdersModel;
import com.example.zone.databinding.ItemOrderBinding;
import com.example.zone.viewModels.ItemOrderViewModel;

import java.util.List;


/**
 * Created by hp on 3/18/2019.
 */
public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ReportViewHolder> {

    private final Activity activity;
    private List<OrdersModel.ResultBean.UserBean> items;

    private boolean isFromSearch;

    public OrdersAdapter(Activity activity, List<OrdersModel.ResultBean.UserBean> items, boolean isFromSearch) {
        this.items = items;
        this.activity = activity;
        this.isFromSearch = isFromSearch;
    }

    @Override
    public ReportViewHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        ItemOrderBinding itemReportBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_order, parent, false);
        return new ReportViewHolder(itemReportBinding);
    }

    @Override
    public void onBindViewHolder(ReportViewHolder holder, final int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    public class ReportViewHolder extends RecyclerView.ViewHolder {

        private ItemOrderBinding itemReportBinding;

        public ReportViewHolder(ItemOrderBinding itemReportBinding) {
            super(itemReportBinding.getRoot());
            this.itemReportBinding = itemReportBinding;
        }

        public void bind(OrdersModel.ResultBean.UserBean report) {
            ItemOrderViewModel itemOrderViewModel = new ItemOrderViewModel(activity, report, isFromSearch);
            itemReportBinding.setRivm(itemOrderViewModel);
        }
    }
}