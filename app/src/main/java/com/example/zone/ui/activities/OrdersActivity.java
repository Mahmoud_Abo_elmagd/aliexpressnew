package com.example.zone.ui.activities;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;

import com.example.zone.R;
import com.example.zone.databinding.ActivityOrdersBinding;
import com.example.zone.viewModels.OrdersViewModel;

public class OrdersActivity extends BaseActivity {

    private ActivityOrdersBinding ordersBinding;

    private OrdersViewModel ordersViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ordersBinding = DataBindingUtil.setContentView(this, R.layout.activity_orders);

        Bundle bundle = getIntent().getBundleExtra("data");

        ordersBinding.titleTxt.setText(bundle.getString("title"));

        ordersViewModel = new OrdersViewModel(this, bundle.getInt("status"), ordersBinding.viewPager, getSupportFragmentManager());
        ordersBinding.setPovm(ordersViewModel);
    }

}
