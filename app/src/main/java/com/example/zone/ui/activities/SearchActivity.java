package com.example.zone.ui.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.zone.R;
import com.example.zone.Utilities.GlobalVariables;
import com.example.zone.Utilities.IntentClass;
import com.example.zone.dataModel.OrdersModel;
import com.example.zone.databinding.ActivitySearchBinding;
import com.example.zone.ui.adapter.OrdersAdapter;
import com.example.zone.ui.adapter.ReturnedOrdersAdapter;
import com.example.zone.viewModels.SearchViewModel;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends BaseActivity {

    public ActivitySearchBinding binding;

    private SearchViewModel viewModel;

    public List<OrdersModel.ResultBean.UserBean> listData = new ArrayList<>();

    public OrdersAdapter ordersAdapter;

    public int nextPage = 1;
    public boolean isLastPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_search);

        initializeRecyclerViews();

        Bundle bundle = getIntent().getBundleExtra("data");

        boolean fromHome = bundle.getBoolean("fromHome");

        viewModel = new SearchViewModel(this);

        binding.setPvm(viewModel);

        if (fromHome){
            openQrCodeActivity();
        }

        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        binding.barcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openQrCodeActivity();
            }
        });

    }

    private void openQrCodeActivity(){
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(SearchActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(SearchActivity.this,
                        new String[]{Manifest.permission.CAMERA
                        }, 2);
            } else {
                IntentClass.goToStartForResult(SearchActivity.this, QRActivity.class,101, null);
            }
        } else {
            IntentClass.goToStartForResult(SearchActivity.this, QRActivity.class, 101, null);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (GlobalVariables.FINISH_ACTIVITY)
            finish();
    }

    private void initializeRecyclerViews() {

        ordersAdapter = new OrdersAdapter(this, listData, true);
        binding.recyclerOrders.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerOrders.setAdapter(ordersAdapter);
        binding.nestedScroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        //code to fetch more data for endless scrolling
                        try {
                            if (!isLastPage) {
                                viewModel.getData(viewModel.searchTxt, nextPage);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });

    }

    public void updateRecycler(List<OrdersModel.ResultBean.UserBean> dataBean) {
        if (nextPage == 1)
            listData.clear();
        listData.addAll(dataBean);
        ordersAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 101) {
            if (!data.getExtras().getString("qr").equals("none")) {

                nextPage = 1;
                isLastPage = false;
                listData.clear();
                ordersAdapter.notifyDataSetChanged();

                binding.EditTextUserName.setText(data.getExtras().getString("qr"));
                viewModel.getData(data.getExtras().getString("qr"), 1);
            }
        }

    }
}