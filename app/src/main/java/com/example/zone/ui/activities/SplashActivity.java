package com.example.zone.ui.activities;

import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.example.zone.R;
import com.example.zone.SharedPreferences.ConfigurationFile;
import com.example.zone.SharedPreferences.LoginSession;
import com.example.zone.Utilities.IntentClass;
import com.example.zone.databinding.ActivitySplashBinding;

public class SplashActivity extends BaseActivity {

    private ActivitySplashBinding activitySplashBinding;
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySplashBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        activity = this;

        ConfigurationFile.setCurrentLanguage(this, "ar");

        Animation animation = AnimationUtils.loadAnimation(activity, R.anim.fade_in);
        activitySplashBinding.logo.startAnimation(animation);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (LoginSession.isLoggedIn(activity))
                    IntentClass.goToActivityAndClear(SplashActivity.this, MainActivity.class, null, R.anim.fade_in_slow, R.anim.fade_out_slow);
                else
                    IntentClass.goToActivityAndClear(SplashActivity.this, LoginActivity.class, null, R.anim.fade_in_slow, R.anim.fade_out_slow);
            }
        }, 2000);
    }

}