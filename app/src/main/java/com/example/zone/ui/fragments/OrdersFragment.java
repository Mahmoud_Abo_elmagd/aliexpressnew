package com.example.zone.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.widget.NestedScrollView;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.zone.R;
import com.example.zone.dataModel.OrdersModel;
import com.example.zone.databinding.FragmentOrdersBinding;
import com.example.zone.ui.adapter.OrdersAdapter;
import com.example.zone.viewModels.OffersViewModel;

import java.util.ArrayList;
import java.util.List;


public class OrdersFragment extends Fragment {

    public FragmentOrdersBinding binding;

    private OffersViewModel offersViewModel;

    private int fltr;
    int status;

    private List<OrdersModel.ResultBean.UserBean> listData = new ArrayList<>();

    private OrdersAdapter ordersAdapter;

    public int nextPage = 1;
    public boolean isLastPage;

    public OrdersFragment(int fltr, int status) {
        this.fltr = fltr;
        this.status = status;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_orders, container, false);

        initializeRecyclerViews();

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();

        nextPage = 1;
        isLastPage = false;

        listData.clear();
        ordersAdapter.notifyDataSetChanged();

        offersViewModel = new OffersViewModel(this, fltr, status);

        binding.setPvm(offersViewModel);
    }

    private void initializeRecyclerViews() {
        ordersAdapter = new OrdersAdapter(getActivity(), listData, false);
        binding.recyclerOrders.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recyclerOrders.setAdapter(ordersAdapter);
        binding.nestedScroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        //code to fetch more data for endless scrolling
                        try {
                            if (!isLastPage) {
                                offersViewModel.getData(nextPage);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });

    }

    public void updateRecycler(List<OrdersModel.ResultBean.UserBean> dataBean) {
        if (nextPage == 1)
            listData.clear();
        listData.addAll(dataBean);
        ordersAdapter.notifyDataSetChanged();
    }
}