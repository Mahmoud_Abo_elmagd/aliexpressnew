package com.example.zone.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.zone.R;
import com.example.zone.Utilities.GlobalVariables;
import com.example.zone.Utilities.Utilities;
import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QRActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private me.dm7.barcodescanner.zxing.ZXingScannerView mScannerView;
    private ImageView back;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.activity_qr);
        Utilities.hideKeyboard(this);
        initView();
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        // Log.v("tag", rawResult.getText()); // Prints scan results
        // Log.v("tag", rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)

        Toast.makeText(this, rawResult.getText(), Toast.LENGTH_SHORT).show();
        //onBackPressed();

        Intent returnIntent = new Intent();
        returnIntent.putExtra("qr", rawResult.getText());
        setResult(Activity.RESULT_OK, returnIntent);
        finish();

    }

    private void initView() {

        mScannerView = (ZXingScannerView) findViewById(R.id.ZXingScannerView);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("qr", "none");
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });
    }
}