package com.example.zone.ui.activities;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;

import com.example.zone.R;
import com.example.zone.databinding.ActivityRecieveOrdersBinding;
import com.example.zone.viewModels.ReceiveOrdersViewModel;

public class RecieveOrdersActivity extends BaseActivity {

    public ActivityRecieveOrdersBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_recieve_orders);

        Bundle bundle = getIntent().getBundleExtra("data");
        binding.toolbarTitle.setText(bundle.getString("title"));

        binding.setPvm(new ReceiveOrdersViewModel(this));
    }

}