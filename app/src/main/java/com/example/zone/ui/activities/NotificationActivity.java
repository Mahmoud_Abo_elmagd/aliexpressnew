package com.example.zone.ui.activities;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;

import com.example.zone.R;
import com.example.zone.databinding.ActivityNotificationBinding;
import com.example.zone.viewModels.NotificationsViewModel;

public class NotificationActivity extends BaseActivity {

    public ActivityNotificationBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification);

        binding.setPvm(new NotificationsViewModel(this));
    }

}