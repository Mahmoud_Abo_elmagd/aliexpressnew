package com.example.zone.ui.activities;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;

import com.example.zone.R;
import com.example.zone.databinding.ActivityLoginBinding;
import com.example.zone.viewModels.LoginViewModel;

public class LoginActivity extends BaseActivity {

    public ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        binding.setLvm(new LoginViewModel(LoginActivity.this));
    }
}
