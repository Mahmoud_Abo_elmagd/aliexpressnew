package com.example.zone.ui.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.zone.R;
import com.example.zone.dataModel.ReceiveModel;
import com.example.zone.databinding.ItemReceiveOrderBinding;
import com.example.zone.viewModels.ItemReceiveOrderViewModel;

import java.util.List;


/**
 * Created by hp on 3/18/2019.
 */
public class ReceiveOrdersAdapter extends RecyclerView.Adapter<ReceiveOrdersAdapter.ReportViewHolder> {

    private final Activity activity;
    private List<ReceiveModel.ResultBean.UserBean> items;

    public ReceiveOrdersAdapter(Activity activity, List<ReceiveModel.ResultBean.UserBean> items) {
        this.items = items;
        this.activity = activity;
    }

    @Override
    public ReportViewHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        ItemReceiveOrderBinding itemReportBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_receive_order, parent, false);
        return new ReportViewHolder(itemReportBinding);
    }

    @Override
    public void onBindViewHolder(ReportViewHolder holder, final int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    public class ReportViewHolder extends RecyclerView.ViewHolder {

        private ItemReceiveOrderBinding itemReportBinding;

        public ReportViewHolder(ItemReceiveOrderBinding itemReportBinding) {
            super(itemReportBinding.getRoot());
            this.itemReportBinding = itemReportBinding;
        }

        public void bind(ReceiveModel.ResultBean.UserBean report) {
            ItemReceiveOrderViewModel itemReceiveOrderViewModel = new ItemReceiveOrderViewModel(activity, report);
            itemReportBinding.setRivm(itemReceiveOrderViewModel);
        }
    }
}