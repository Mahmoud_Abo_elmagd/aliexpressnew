package com.example.zone.ui.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.zone.R;
import com.example.zone.dataModel.OrdersModel;
import com.example.zone.databinding.ItemReturnedOrderBinding;
import com.example.zone.viewModels.ItemReturnedOrderViewModel;

import java.util.List;


/**
 * Created by hp on 3/18/2019.
 */
public class ReturnedOrdersAdapter extends RecyclerView.Adapter<ReturnedOrdersAdapter.ReturnedViewHolder> {

    private final Activity activity;
    private List<OrdersModel.ResultBean.UserBean> items;

    public ReturnedOrdersAdapter(Activity activity, List<OrdersModel.ResultBean.UserBean> items) {
        this.items = items;
        this.activity = activity;
    }

    @Override
    public ReturnedViewHolder onCreateViewHolder(ViewGroup parent,
                                                 int viewType) {
        ItemReturnedOrderBinding itemReturnedOrderBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_returned_order, parent, false);
        return new ReturnedViewHolder(itemReturnedOrderBinding);
    }

    @Override
    public void onBindViewHolder(ReturnedViewHolder holder, final int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    public class ReturnedViewHolder extends RecyclerView.ViewHolder {

        private ItemReturnedOrderBinding itemReturnedOrderBinding;

        public ReturnedViewHolder(ItemReturnedOrderBinding itemReturnedOrderBinding) {
            super(itemReturnedOrderBinding.getRoot());
            this.itemReturnedOrderBinding = itemReturnedOrderBinding;
        }

        public void bind(OrdersModel.ResultBean.UserBean order) {
            ItemReturnedOrderViewModel itemOrderViewModel = new ItemReturnedOrderViewModel(activity, order);
            itemReturnedOrderBinding.setRivm(itemOrderViewModel);
        }
    }
}