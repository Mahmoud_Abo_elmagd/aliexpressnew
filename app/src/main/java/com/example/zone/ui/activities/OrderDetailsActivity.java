package com.example.zone.ui.activities;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;

import com.example.zone.R;
import com.example.zone.dataModel.OrdersModel;
import com.example.zone.databinding.ActivityOrderDetailsBinding;
import com.example.zone.viewModels.OrderDetailsViewModel;

public class OrderDetailsActivity extends BaseActivity {

    private ActivityOrderDetailsBinding ordersDetailsBinding;
    private OrdersModel.ResultBean.UserBean orderModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ordersDetailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_order_details);
        Bundle bundle = getIntent().getBundleExtra("data");
        orderModel = (OrdersModel.ResultBean.UserBean) bundle.getSerializable("orderModel");

        boolean isFromSearch = bundle.getBoolean("isFromSearch");

        ordersDetailsBinding.setOrderDetailsViewModel(new OrderDetailsViewModel(this, orderModel, isFromSearch));
    }
}