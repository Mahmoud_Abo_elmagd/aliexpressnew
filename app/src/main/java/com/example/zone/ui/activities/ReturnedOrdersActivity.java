package com.example.zone.ui.activities;

import android.os.Bundle;

import androidx.core.widget.NestedScrollView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.zone.R;
import com.example.zone.dataModel.OrdersModel;
import com.example.zone.databinding.ActivityReturnedOrdersBinding;
import com.example.zone.ui.adapter.ReturnedOrdersAdapter;
import com.example.zone.viewModels.ReturnedOrdersViewModel;

import java.util.ArrayList;
import java.util.List;

public class ReturnedOrdersActivity extends BaseActivity {

    private ReturnedOrdersViewModel returnedOrdersViewModel;

    public ActivityReturnedOrdersBinding binding;

    public List<OrdersModel.ResultBean.UserBean> listData = new ArrayList<>();

    public ReturnedOrdersAdapter returnedOrdersAdapter;

    public int nextPage = 1;
    public boolean isLastPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_returned_orders);

        Bundle bundle = getIntent().getBundleExtra("data");
        binding.toolbarTitle.setText(bundle.getString("title"));

        initializeRecyclerViews();

        returnedOrdersViewModel = new ReturnedOrdersViewModel(this, bundle.getInt("status"));

        binding.setPvm(returnedOrdersViewModel);
    }

    private void initializeRecyclerViews() {
        returnedOrdersAdapter = new ReturnedOrdersAdapter(this, listData);
        binding.recyclerOrders.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerOrders.setAdapter(returnedOrdersAdapter);
        binding.nestedScroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        //code to fetch more data for endless scrolling
                        try {
                            if (!isLastPage) {
                                if (returnedOrdersViewModel.isSearch)
                                    returnedOrdersViewModel.getData(nextPage);
                                else
                                    returnedOrdersViewModel.getData(returnedOrdersViewModel.searchTxt, nextPage);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });

    }

    public void updateRecycler(List<OrdersModel.ResultBean.UserBean> dataBean) {
        if (nextPage == 1)
            listData.clear();
        listData.addAll(dataBean);
        returnedOrdersAdapter.notifyDataSetChanged();
    }

}