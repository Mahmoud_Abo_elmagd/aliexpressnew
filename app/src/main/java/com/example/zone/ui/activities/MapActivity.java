package com.example.zone.ui.activities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.example.zone.API.APIModel;
import com.example.zone.R;
import com.example.zone.Utilities.Dialogs;
import com.example.zone.Utilities.LoadingDialog;
import com.example.zone.Utilities.Utilities;
import com.example.zone.dataModel.LocationsListModel;
import com.example.zone.databinding.ActivityMapBinding;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MapActivity extends BaseActivity implements OnMapReadyCallback {

    public ActivityMapBinding binding;

    private GoogleApiClient mGoogleApiClient;

    private LatLng latLngZooming = new LatLng(30, 31);

    private GoogleMap mMap;

    private List<LocationsListModel.ResultBean.UserBean> listMarkers = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_map);

        Bundle bundle = getIntent().getBundleExtra("data");

        binding.toolbarTitle.setText(bundle.getString("title"));

        binding.frameBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getData();

        setUpMapIfNeeded();

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        setMapLocation();
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        99);
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        99);
            }
        }
    }

    private void setMapLocation() {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    public Marker createMarker(String position, String latitude, String longitude, String name, String image) {

        if (!latitude.equals("") || !longitude.equals("")) {
            Double lat = Double.parseDouble(latitude);
            Double lang = Double.parseDouble(longitude);

            return mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(lat, lang))
                    .anchor(0.5f, 0.5f)
                    .snippet(position)
                    .title(name)
                    .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(image))));
        } else return null;
    }

    public void AddMarkerForLoop(List<LocationsListModel.ResultBean.UserBean> list) {
        listMarkers.addAll(list);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                try {
                    String lat = String.valueOf(list.get(i).getUserLat());
                    String lng = String.valueOf(list.get(i).getUserLng());
                    createMarker(i + "", lat, lng, list.get(i).getFullName(), list.get(i).getImgUrl());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Bitmap getMarkerBitmapFromView(String image) {
        View customMarkerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_view_home_marker, null);

        CircleImageView icon_img = customMarkerView.findViewById(R.id.icon_img);

        Picasso.get().load(image).placeholder(R.drawable.ic_user).into(icon_img);

        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);

        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);
        return returnedBitmap;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        if (mMap != null) {
            mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
            mMap.getUiSettings().setZoomControlsEnabled(false);
            mMap.getUiSettings().setCompassEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.getUiSettings().setZoomGesturesEnabled(true);
            mMap.getUiSettings().setRotateGesturesEnabled(true);
            mMap.getUiSettings().setMapToolbarEnabled(false);
        }

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLngZooming).zoom(11).build();

        mMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));

        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                View markerItemView = getLayoutInflater().inflate(R.layout.marker_info_window, null);  // 1

                String markerPos = marker.getId();
                String marPosInt = markerPos.substring(1);
                TextView store_name = markerItemView.findViewById(R.id.store_name);
                store_name.setText(listMarkers.get(Integer.parseInt(marPosInt)).getFullName());
                return markerItemView;  // 4
            }

            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }
        });


        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                marker.hideInfoWindow();

            }
        });

    }

    private void getData() {
        final LoadingDialog loadingDialog = new LoadingDialog();

        APIModel.getMethod(this, "Delivery/displaylocation", new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                Log.e("response", responseString + "Error");
                switch (statusCode) {
                    case 400:
                        try {
                            JSONObject jsonObject = new JSONObject(responseString);
                            if (jsonObject.has("error"))
                                Utilities.toastyError(MapActivity.this, jsonObject.getJSONObject("error").getString("Message"));
                            else
                                Utilities.toastyError(MapActivity.this, responseString + "    ");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        APIModel.handleFailure(MapActivity.this, statusCode, responseString, new APIModel.RefreshTokenListener() {
                            @Override
                            public void onRefresh() {
                                getData();
                            }
                        });
                        break;
                }
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                Log.e("response", responseString);

                Type dataType = new TypeToken<LocationsListModel>() {
                }.getType();
                LocationsListModel data = new Gson().fromJson(responseString, dataType);

                if (data.getResult().getUser().size() > 0) {
                    latLngZooming = new LatLng(
                            Double.valueOf(data.getResult().getUser().get(0).getUserLat()),
                            Double.valueOf(data.getResult().getUser().get(0).getUserLng())
                    );

                    AddMarkerForLoop(data.getResult().getUser());

                    try {
                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(latLngZooming).zoom(11).build();
                        mMap.animateCamera(CameraUpdateFactory
                                .newCameraPosition(cameraPosition));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onStart() {
                super.onStart();
                Dialogs.showLoading(MapActivity.this, loadingDialog);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                Dialogs.dismissLoading(loadingDialog);
            }
        });
    }

}