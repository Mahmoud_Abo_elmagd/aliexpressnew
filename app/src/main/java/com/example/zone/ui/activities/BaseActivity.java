package com.example.zone.ui.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.zone.R;
import com.example.zone.SharedPreferences.ConfigurationFile;
import com.example.zone.Utilities.Utilities;

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ConfigurationFile.setCurrentLanguage(this, ConfigurationFile.getCurrentLanguage(this));
        Utilities.changeStatusBarColor(this,getResources().getColor(R.color.colorBlackOp20),true);
    }
}