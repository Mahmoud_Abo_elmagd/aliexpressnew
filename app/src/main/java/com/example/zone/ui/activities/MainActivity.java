package com.example.zone.ui.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.anychart.AnyChart;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Pie;
import com.example.zone.API.APIModel;
import com.example.zone.R;
import com.example.zone.SharedPreferences.ConfigurationFile;
import com.example.zone.SharedPreferences.LoginSession;
import com.example.zone.Utilities.GlobalVariables;
import com.example.zone.databinding.ActivityMainBinding;
import com.example.zone.viewModels.MainViewModel;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity {

    private ActivityMainBinding activityMainBinding;

    private MainViewModel mainViewModel;

    public LatLng latLngTo = new LatLng(0, 0);
    private FusedLocationProviderClient fusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);


        anyChartTest();

        ConfigurationFile.setCurrentLanguage(this, "ar");


        activityMainBinding.username.setText(LoginSession.getUserData(this).getResult().getUser().getFullName());
        activityMainBinding.rolePermission.setText(LoginSession.getUserData(this).getResult().getUser().getJobLNm());

        Picasso.get().load(APIModel.BASE_URL +LoginSession.getUserData(this).getResult().getUser().getImgUrl()).placeholder(R.drawable.ic_user).into(activityMainBinding.userImg);

        mainViewModel = new MainViewModel(this);
        activityMainBinding.setMvm(mainViewModel);

        mainViewModel.getUserPermission();
        mainViewModel.updateFCM(this);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (latLngTo.latitude != 0 && latLngTo.longitude != 0) {
                    try {
                        mainViewModel.updateLocation(MainActivity.this, latLngTo);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }, 1000 * 60 * 2);
    }

    @Override
    protected void onResume() {
        super.onResume();

        GlobalVariables.FINISH_ACTIVITY = false;

        mainViewModel.onResume();

    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 99);
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 99);
        }
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            try {
                                latLngTo = new LatLng(location.getLatitude(), location.getLongitude());
                                mainViewModel.updateLocation(MainActivity.this, latLngTo);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            Log.e("fusedLocation", location.getLatitude() + " " + location.getLongitude());
                        }
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        try {
            if (requestCode == 99) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                    fusedLocationClient.getLastLocation()
                            .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {
                                    // Got last known location. In some rare situations this can be null.
                                    if (location != null) {

                                        latLngTo = new LatLng(location.getLatitude(), location.getLongitude());

                                        mainViewModel.updateLocation(MainActivity.this, latLngTo);
                                    }
                                }
                            });
                }

            } else {
                fusedLocationClient.getLastLocation()
                        .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                // Got last known location. In some rare situations this can be null.
                                if (location != null) {

                                    latLngTo = new LatLng(location.getLatitude(), location.getLongitude());
                                    mainViewModel.updateLocation(MainActivity.this, latLngTo);
                                }
                            }
                        });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void anyChartTest(){

        activityMainBinding.anyChartView.setProgressBar(activityMainBinding.progrsView);

        Pie pie = AnyChart.pie();

//        pie.setOnClickListener(new ListenersInterface.OnClickListener(new String[]{"x", "value"}) {
//            @Override
//            public void onClick(Event event) {
//                Toast.makeText(MainActivity.this, event.getData().get("x") + ":" + event.getData().get("value"), Toast.LENGTH_SHORT).show();
//            }
//        });

        List<DataEntry> data = new ArrayList<>();
//        data.add(new ValueDataEntry("بيكـ اب", Integer.valueOf(mainViewModel.pickupVal.get())));
//        data.add(new ValueDataEntry("شحنات قيد التوصيل", Integer.valueOf(mainViewModel.deliveringVal.get())));
//        data.add(new ValueDataEntry("شحنات مرتجعة", Integer.valueOf(mainViewModel.returnedVal.get())));
//        data.add(new ValueDataEntry("تسليم جزئي", Integer.valueOf(mainViewModel.partialVal.get())));
//        data.add(new ValueDataEntry("شحنات مؤجلة", Integer.valueOf(mainViewModel.postponedVal.get())));
//        data.add(new ValueDataEntry("شحنات ملغاه", Integer.valueOf(mainViewModel.canceledVal.get())));
//        data.add(new ValueDataEntry("شحنات تم تسليمها", Integer.valueOf(mainViewModel.deliveredVal.get())));

        data.add(new ValueDataEntry("بيكـ اب", 2));
        data.add(new ValueDataEntry("شحنات قيد التوصيل", 5));
        data.add(new ValueDataEntry("شحنات مرتجعة", 1));
        data.add(new ValueDataEntry("تسليم جزئي", 2));
        data.add(new ValueDataEntry("شحنات مؤجلة", 3));
        data.add(new ValueDataEntry("شحنات ملغاه", 1));
        data.add(new ValueDataEntry("شحنات تم تسليمها", 6));

        pie.data(data);

        //pie.title("كل الطلبات");

        pie.labels().position("outside");

        pie.animation().enabled(true).duration(1000);

//        pie.legend().title().enabled(true);
//        pie.legend().title()
//                .text("نسب توصيل الطلبات")
//                .padding(0d, 0d, 10d, 0d);
//
//        pie.legend()
//                .position("center-bottom")
//                .itemsLayout(LegendLayout.HORIZONTAL)
//                .align(Align.CENTER);

        activityMainBinding.anyChartView.setChart(pie);

    }
}

