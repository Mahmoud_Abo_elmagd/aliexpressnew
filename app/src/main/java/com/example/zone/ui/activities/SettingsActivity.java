package com.example.zone.ui.activities;

import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.example.zone.R;
import com.example.zone.SharedPreferences.LoginSession;
import com.example.zone.Utilities.IntentClass;
import com.example.zone.databinding.ActivitySettingsBinding;

public class SettingsActivity extends BaseActivity {

    ActivitySettingsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_settings);
        onClick();
    }


    void onClick() {


        binding.LogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginSession.clearData(SettingsActivity.this);
                IntentClass.goToActivityAndClear(SettingsActivity.this, LoginActivity.class, null);
            }
        });

        binding.editBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //IntentClass.goToActivity(SettingsActivity.this, AddBannerActivity.class, null);
            }
        });
        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingsActivity.this.finish();
            }
        });

    }
}