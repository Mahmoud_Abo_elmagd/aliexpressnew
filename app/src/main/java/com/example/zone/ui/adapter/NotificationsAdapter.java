package com.example.zone.ui.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.zone.R;
import com.example.zone.dataModel.NotificationModel;
import com.example.zone.databinding.ItemNotificationsBinding;
import com.example.zone.viewModels.ItemNotificationViewModel;

import java.util.List;


/**
 * Created by hp on 3/18/2019.
 */
public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.NotificationsViewHolder> {

    private final Activity activity;
    private List<NotificationModel.ResultBean.UserBean> items;

    public NotificationsAdapter(Activity activity, List<NotificationModel.ResultBean.UserBean> items) {
        this.items = items;
        this.activity = activity;
    }

    @Override
    public NotificationsViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        ItemNotificationsBinding itemNotificationsBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_notifications, parent, false);
        return new NotificationsViewHolder(itemNotificationsBinding);
    }

    @Override
    public void onBindViewHolder(NotificationsViewHolder holder, final int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    public class NotificationsViewHolder extends RecyclerView.ViewHolder {

        private ItemNotificationsBinding itemReportBinding;

        public NotificationsViewHolder(ItemNotificationsBinding itemReportBinding) {
            super(itemReportBinding.getRoot());
            this.itemReportBinding = itemReportBinding;
        }

        public void bind(NotificationModel.ResultBean.UserBean notification) {
            ItemNotificationViewModel itemNotificationViewModel = new ItemNotificationViewModel(notification);
            itemReportBinding.setRivm(itemNotificationViewModel);
        }
    }
}