package com.example.zone.ui.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.zone.API.APIModel;
import com.example.zone.R;
import com.example.zone.SharedPreferences.LoginSession;
import com.example.zone.Utilities.Dialogs;
import com.example.zone.Utilities.GlobalVariables;
import com.example.zone.Utilities.LoadingDialog;
import com.example.zone.Utilities.Utilities;
import com.example.zone.dataModel.OrdersModel;
import com.example.zone.ui.activities.OrderDetailsActivity;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ChangeStatusBottomSheet extends BottomSheetDialogFragment {

    private OrderDetailsActivity activity;

    private OrdersModel.ResultBean.UserBean orderModel;

    private TextInputEditText editTextUserName;
    private TextInputEditText editTextCurrency;
    private TextInputEditText edt_cancelAmt;
    private TextView dateTxt;

    private int visibilities, status;


   /*
    visibilities --> 1   delivered
    visibilities --> 2   part
    visibilities --> 3   cancel
    visibilities --> 4   postpone
    */

    private int sender_receiver = 0;
    private String shippPer = "";
    private String selectedReason = "";

    private final List<String> personTypeList = new ArrayList<>();

    private boolean isSpinner = false;

    public ChangeStatusBottomSheet() {
    }

    @SuppressLint("ValidFragment")
    public ChangeStatusBottomSheet(OrderDetailsActivity activity, OrdersModel.ResultBean.UserBean orderModel, int visibilities, int status) {
        this.activity = activity;
        this.orderModel = orderModel;
        this.visibilities = visibilities;
        this.status = status;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(activity, R.layout.change_status_bottom_sheet, null);
        dialog.setContentView(contentView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;

                FrameLayout bottomSheet = (FrameLayout) d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        final LinearLayout confirm = dialog.findViewById(R.id.confirm);
        final LinearLayout cancelLinear = dialog.findViewById(R.id.cancel_linear);
        final LinearLayout lin_spinner = dialog.findViewById(R.id.lin_spinner);
        dateTxt = dialog.findViewById(R.id.date_txt);
        editTextUserName = dialog.findViewById(R.id.EditTextUserName);
        editTextCurrency = dialog.findViewById(R.id.EditTextCurrency);
        TextInputLayout input_layout = dialog.findViewById(R.id.input_layout);
        final TextInputLayout edt_notes = dialog.findViewById(R.id.edt_notes);
        RadioButton radioSender = dialog.findViewById(R.id.radio_sender);
        RadioButton radioReciever = dialog.findViewById(R.id.radio_reciever);
        edt_cancelAmt = dialog.findViewById(R.id.EditTextCancelAmt);
        Spinner spinnerReason = dialog.findViewById(R.id.spinner_reason);

        edt_cancelAmt.setText(String.valueOf(orderModel.getOrd_Ship_Discount()));

        personTypeList.add(getResources().getString(R.string.choose_reason));
        personTypeList.add(getResources().getString(R.string.choose_reason_2));
        personTypeList.add(getResources().getString(R.string.choose_reason_3));
        personTypeList.add(getResources().getString(R.string.choose_reason_4));
        personTypeList.add(getResources().getString(R.string.choose_reason_5));
        personTypeList.add(getResources().getString(R.string.other));
        ArrayAdapter<String> adapterPersonType = new ArrayAdapter(activity, android.R.layout.simple_spinner_item, personTypeList);
        adapterPersonType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerReason.setAdapter(adapterPersonType);
        spinnerReason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedReason = personTypeList.get(position);

                if (selectedReason.equals(activity.getString(R.string.other))) {
                    edt_notes.setVisibility(View.VISIBLE);
                    isSpinner = false;
                } else {
                    edt_notes.setVisibility(View.GONE);
                    isSpinner = true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        radioSender.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    sender_receiver = 2;
            }
        });

        radioReciever.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    sender_receiver = 1;
            }
        });

        if (visibilities == 1) {
            edt_notes.setVisibility(View.VISIBLE);
        } else if (visibilities == 2) {
            edt_notes.setVisibility(View.VISIBLE);
            input_layout.setVisibility(View.VISIBLE);
        } else if (visibilities == 3) {
            isSpinner = true;
            cancelLinear.setVisibility(View.VISIBLE);
            lin_spinner.setVisibility(View.VISIBLE);
        } else if (visibilities == 4) {
            isSpinner = true;
            dateTxt.setVisibility(View.VISIBLE);
            lin_spinner.setVisibility(View.VISIBLE);
        }

        dateTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment mDatePicker = new DatePickerFragment(activity, dateTxt);
                mDatePicker.show(activity.getSupportFragmentManager(), "Select date");
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isSpinner)
                    selectedReason = editTextUserName.getText().toString();

                shippPer = edt_cancelAmt.getText().toString();

                Log.e("reason", selectedReason + " reason");

                if (visibilities == 1) {
                    if (selectedReason.isEmpty()) {
                        editTextUserName.setError("هذا الحقل مطلوب");
                    } else {
                        changeStatus(selectedReason);
                    }
                } else if (visibilities == 2) {
                    if (selectedReason.isEmpty() || editTextCurrency.getText().toString().isEmpty() ||
                            Double.parseDouble(editTextCurrency.getText().toString()) > orderModel.getOrd_Total_Price()) {

                        if (selectedReason.isEmpty())
                            editTextUserName.setError("هذا الحقل مطلوب");

                        if (editTextCurrency.getText().toString().isEmpty())
                            editTextCurrency.setError("هذا الحقل مطلوب");

                        try {

                            if (Double.parseDouble(editTextCurrency.getText().toString()) > orderModel.getOrd_Total_Price()) {
                                editTextCurrency.setError("يجب ان تكون القيمة المدخلة اقل من إجمالى الطلب");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        changeStatus(selectedReason);
                    }
                } else if (visibilities == 3) {

                    boolean flag = true;

                    if (selectedReason.isEmpty()) {
                        flag = false;
                        editTextUserName.setError("هذا الحقل مطلوب");
                        Log.e("validation", "3 " + selectedReason);
                    }
                    if (sender_receiver == 0 || shippPer.isEmpty()) {
                        flag = false;
                        if (sender_receiver == 0) {
                            Utilities.toastyError(activity, "برجاء تحديد نوع الشحن على");
                        }
                        if (shippPer.isEmpty()) {
                            edt_cancelAmt.setError("برجاء تحديد قيمة الشحن");
                        }
                    }

                    if (flag)
                        changeStatus(selectedReason);
                } else if (visibilities == 4) {
                    if (selectedReason.isEmpty() || dateTxt.getText().toString().isEmpty()) {
                        if (selectedReason.isEmpty()) {
                            editTextUserName.setError("هذا الحقل مطلوب");

                            Log.e("validation", "4 " + selectedReason);
                        }

                        if (dateTxt.getText().toString().isEmpty())
                            Utilities.toastyError(activity, "برجاء تحديد التاريخ");

                    } else {
                        changeStatus(selectedReason);
                    }
                }
            }
        });
    }

    private void changeStatus(final String notes) {
        final LoadingDialog loadingDialog = new LoadingDialog();


        // ShippType   في حالة المستلم ShippType = 1         في حالة الراسل ShippType = 2
        // ShippPer   في حالة 100 %     ShippPer = 2     في حالة 50 %    ShippPer = 3      في حالة 0%       ShippPer = 4

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("RecordSerial", orderModel.getRecordSerial());
            jsonParams.put("OrderSheetID", orderModel.getOrderSheetID());
            jsonParams.put("DeliveryID", LoginSession.getUserData(activity).getResult().getUser().getEmployeeID());
            jsonParams.put("StatusID", status);
            jsonParams.put("ClientID", orderModel.getClientID());

            if (visibilities == 2) {
                jsonParams.put("Ord_Pay_Amt", editTextCurrency.getText().toString());
            } else if (visibilities == 4) {
                jsonParams.put("Delivery_Date", dateTxt.getText().toString()); // date format 02/11/2020
            } else if (visibilities == 3) {
                jsonParams.put("ShippType", sender_receiver);
                jsonParams.put("Cancel_Shipp_Amt", shippPer);
            }

            jsonParams.put("Ord_Note", notes);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        APIModel.postMethod(activity, "Delivery/feadbackaction", jsonParams, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                Log.e("response", responseString + "Error");
                switch (statusCode) {
                    case 400:
                        try {
                            JSONObject jsonObject = new JSONObject(responseString);
                            if (jsonObject.has("error"))
                                Utilities.toastyError(activity, jsonObject.getJSONObject("error").getString("Message"));
                            else
                                Utilities.toastyError(activity, responseString + "    ");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        APIModel.handleFailure(activity, statusCode, responseString, new APIModel.RefreshTokenListener() {
                            @Override
                            public void onRefresh() {
                                changeStatus(notes);
                            }
                        });
                        break;
                }
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                Log.e("response", responseString);

                try {
                    JSONObject jsonObject = new JSONObject(responseString);
                    if (jsonObject.has("result"))
                        Utilities.toastyError(activity, jsonObject.getJSONObject("result").getString("Message"));
                    else
                        Utilities.toastyError(activity, responseString + "    ");
                } catch (JSONException e2) {
                    e2.printStackTrace();
                }

                GlobalVariables.FINISH_ACTIVITY = true;

                activity.finish();
            }

            @Override
            public void onStart() {
                super.onStart();
                Dialogs.showLoading(activity, loadingDialog);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                Dialogs.dismissLoading(loadingDialog);
            }
        });
    }
}