package com.example.zone.viewModels;

import android.util.Log;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.example.zone.API.APIModel;
import com.example.zone.R;
import com.example.zone.SharedPreferences.LoginSession;
import com.example.zone.Utilities.Dialogs;
import com.example.zone.Utilities.GlobalVariables;
import com.example.zone.Utilities.IntentClass;
import com.example.zone.Utilities.LoadingDialog;
import com.example.zone.Utilities.Utilities;
import com.example.zone.dataModel.OrdersModel;
import com.example.zone.ui.activities.OrderDetailsActivity;
import com.example.zone.ui.fragments.ChangeStatusBottomSheet;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

public class OrderDetailsViewModel {
    public ObservableField<OrdersModel.ResultBean.UserBean> orderModelObservable = new ObservableField<>();
    public ObservableBoolean isChangeStatus = new ObservableBoolean(false);
    public ObservableBoolean isOutdoor = new ObservableBoolean(false);
    public ObservableField<String> orderId = new ObservableField<>();
    public ObservableField<String> placedOn = new ObservableField<>();
    public ObservableField<String> orderTotal = new ObservableField<>();
    public ObservableField<String> deleveryCharge = new ObservableField<>();
    public ObservableField<String> totalAmount = new ObservableField<>();
    public ObservableField<String> address = new ObservableField<>();
    public ObservableField<String> city = new ObservableField<>();
    public ObservableField<String> area = new ObservableField<>();
    public ObservableField<String> phone = new ObservableField<>();
    public ObservableField<String> senderPhone = new ObservableField<>();
    public ObservableField<String> date = new ObservableField<>();
    private OrderDetailsActivity activity;

    private String clientPhone;
    private String senderPhoneStr;

    private boolean isFromSearch;

    public OrderDetailsViewModel(OrderDetailsActivity activity, OrdersModel.ResultBean.UserBean orderModel, boolean isFromSearch) {
        this.activity = activity;
        this.isFromSearch = isFromSearch;
        orderModelObservable.set(orderModel);

        //isOutdoor.set(LoginSession.getUserData(activity).getResult().getUser().isIs_OutDoor());

        if (orderModel.getStatusID() == 3 || orderModel.getStatusID() == 6) {
            isChangeStatus.set(true);
        }

        orderId.set(activity.getResources().getString(R.string.order_id) + "     " + orderModel.getPolessa_No());
        placedOn.set(activity.getResources().getString(R.string.product_name) + "     " + orderModel.getClient_Name());
        phone.set(activity.getResources().getString(R.string.product_phone) + "     " + orderModel.getClient_Phone());
        senderPhone.set(activity.getResources().getString(R.string.sender_phone) + "     " + orderModel.getSender_Phone_No());
        date.set(activity.getResources().getString(R.string.product_date) + "     " + orderModel.getDelivery_Date());
        orderTotal.set(orderModel.getOrd_Total_Price() + "      " + activity.getResources().getString(R.string.currency));
        deleveryCharge.set(orderModel.getOrd_Ship_Client() + "      " + activity.getResources().getString(R.string.currency));
        totalAmount.set(orderModel.getOrd_Final_Price() + "      " + activity.getResources().getString(R.string.currency));

        address.set("العنوان"+":- "+orderModel.getClient_Address());
        city.set("المدينة"+":-  "+orderModel.getCity_L_Nm());
        area.set("المنطقة"+":- "+orderModel.getArea_L_Nm());

        clientPhone = orderModel.getClient_Phone();
        senderPhoneStr = orderModel.getSender_Phone_No();
    }

    public void call(int type) {
        if (type == 1)
            IntentClass.goTodialPhoneNumber(activity, clientPhone);
        else
            IntentClass.goTodialPhoneNumber(activity, senderPhoneStr);
    }

    public void back() {
        if (isFromSearch)
            GlobalVariables.FINISH_ACTIVITY = true;
        activity.finish();
    }

    public void delivered() {
        ChangeStatusBottomSheet changeStatusBottomSheet = new ChangeStatusBottomSheet(activity, orderModelObservable.get(), 1, 4);
        changeStatusBottomSheet.show(activity.getSupportFragmentManager(), "tag");
    }

    public void partDelivered() {
        ChangeStatusBottomSheet changeStatusBottomSheet = new ChangeStatusBottomSheet(activity, orderModelObservable.get(), 2, 7);
        changeStatusBottomSheet.show(activity.getSupportFragmentManager(), "tag");
    }

    public void cancelOrder() {
        ChangeStatusBottomSheet changeStatusBottomSheet = new ChangeStatusBottomSheet(activity, orderModelObservable.get(), 3, 5);
        changeStatusBottomSheet.show(activity.getSupportFragmentManager(), "tag");
    }

    public void postponed() {
        ChangeStatusBottomSheet changeStatusBottomSheet = new ChangeStatusBottomSheet(activity, orderModelObservable.get(), 4, 6);
        changeStatusBottomSheet.show(activity.getSupportFragmentManager(), "tag");
    }
}