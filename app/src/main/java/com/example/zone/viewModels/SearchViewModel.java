package com.example.zone.viewModels;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableFloat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.zone.API.APIModel;
import com.example.zone.SharedPreferences.ConfigurationFile;
import com.example.zone.SharedPreferences.LoginSession;
import com.example.zone.Utilities.Dialogs;
import com.example.zone.Utilities.LoadingDialog;
import com.example.zone.Utilities.Utilities;
import com.example.zone.dataModel.OrdersModel;
import com.example.zone.ui.activities.SearchActivity;
import com.example.zone.ui.adapter.OrdersAdapter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;

public class SearchViewModel {

    private SearchActivity activity;

    public ObservableBoolean showNoProductsObservable = new ObservableBoolean(false);
    public ObservableFloat rotationYObservable = new ObservableFloat(0);

    public String searchTxt = "";

    public SearchViewModel(SearchActivity activity) {
        this.activity = activity;

        if (ConfigurationFile.getCurrentLanguage(activity).equals("ar"))
            rotationYObservable.set(180);
    }

    public void getData(final String search, final int page) {
        final LoadingDialog loadingDialog = new LoadingDialog();

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("Client_Name", search);
            jsonParams.put("DeliveryID", LoginSession.getUserData(activity).getResult().getUser().getEmployeeID());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        APIModel.postMethod(activity, "Delivery/searchorderslst?pag=" + page, jsonParams, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                Log.e("response", responseString + "Error");
                showNoProductsObservable.set(true);
                switch (statusCode) {
                    case 400:
                        try {
                            JSONObject jsonObject = new JSONObject(responseString);
                            if (jsonObject.has("error"))
                                Utilities.toastyError(activity, jsonObject.getJSONObject("error").getString("Message"));
                            else
                                Utilities.toastyError(activity, responseString + "    ");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        APIModel.handleFailure(activity, statusCode, responseString, new APIModel.RefreshTokenListener() {
                            @Override
                            public void onRefresh() {
                                getData(search, page);
                            }
                        });
                        break;
                }
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                Log.e("response", responseString);

                Type dataType = new TypeToken<OrdersModel>() {
                }.getType();
                OrdersModel data = new Gson().fromJson(responseString, dataType);

                if (data.getResult().getUser().size() > 0) {
                    activity.isLastPage = false;
                    showNoProductsObservable.set(false);
                    activity.updateRecycler(data.getResult().getUser());
                } else {
                    activity.isLastPage = true;
                    if (activity.nextPage == 1)
                        showNoProductsObservable.set(true);
                }

                activity.nextPage += 1;

//                if (data.getResult().getUser().size() > 0) {
//                    OrdersAdapter reportAdapter = new OrdersAdapter(activity, data.getResult().getUser(), true);
//                    activity.binding.recyclerOrders.setLayoutManager(new LinearLayoutManager(activity));
//                    activity.binding.recyclerOrders.setAdapter(reportAdapter);
//                } else {
//                    showNoProductsObservable.set(true);
//                }

            }

            @Override
            public void onStart() {
                super.onStart();
                Dialogs.showLoading(activity, loadingDialog);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                Dialogs.dismissLoading(loadingDialog);
            }
        });
    }

    public TextWatcher getSearchText() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(final Editable editable) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        activity.nextPage = 1;
                        activity.isLastPage = false;
                        activity.listData.clear();
                        activity.ordersAdapter.notifyDataSetChanged();

                        if (editable.toString().length() > 0){

                            searchTxt = editable.toString();

                            getData(editable.toString(), 1);
                        }else {
                            getData("", 1);
                        }

                    }
                }, 2000);
            }
        };
    }
}
