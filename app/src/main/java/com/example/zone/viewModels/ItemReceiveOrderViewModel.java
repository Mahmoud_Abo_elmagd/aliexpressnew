package com.example.zone.viewModels;

import android.app.Activity;
import android.util.Log;
import android.widget.CheckBox;

import androidx.databinding.ObservableField;

import com.example.zone.API.APIModel;
import com.example.zone.R;
import com.example.zone.SharedPreferences.LoginSession;
import com.example.zone.Utilities.Dialogs;
import com.example.zone.Utilities.IntentClass;
import com.example.zone.Utilities.LoadingDialog;
import com.example.zone.Utilities.Utilities;
import com.example.zone.dataModel.ReceiveModel;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

public class ItemReceiveOrderViewModel {
    public ObservableField<String> status = new ObservableField<>();
    public ObservableField<String> name = new ObservableField<>();
    public ObservableField<String> phone = new ObservableField<>();
    public ObservableField<String> address = new ObservableField<>();
    public ObservableField<String> date = new ObservableField<>();
    public ObservableField<String> money = new ObservableField<>();

    private Activity activity;

    public ObservableField<ReceiveModel.ResultBean.UserBean> orderModelObservable = new ObservableField<>();

    private ReceiveModel.ResultBean.UserBean order;

    public ItemReceiveOrderViewModel(Activity activity, ReceiveModel.ResultBean.UserBean order) {
        this.activity = activity;
        this.order = order;

        orderModelObservable.set(order);

        status.set(order.getReciveNote() + "");
        name.set(order.getClientLNm() );
        phone.set(order.getPhoneNo());
        address.set(order.getClientAddress());
        date.set(order.getReciveDate());
        money.set(activity.getString(R.string.money_to_get) + " " + order.getReqAmtDuo());

    }

    public void call() {
        IntentClass.goTodialPhoneNumber(activity, order.getPhoneNo());
    }

    public void accept() {
        final LoadingDialog loadingDialog = new LoadingDialog();

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("ReqPayment_ID", order.getReqPaymentID());
            jsonParams.put("ReciveRequestUID", order.getReciveRequestUID());
            jsonParams.put("ShippingAmt", order.getShippingAmt());
            jsonParams.put("EmployeeID", order.getEmployeeID());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        APIModel.postMethod(activity, "Delivery/reciveaction", jsonParams, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                Log.e("response", responseString + "Error");
                switch (statusCode) {
                    case 400:
                        try {
                            JSONObject jsonObject = new JSONObject(responseString);
                            if (jsonObject.has("error"))
                                Utilities.toastyError(activity, jsonObject.getJSONObject("error").getString("Message"));
                            else
                                Utilities.toastyError(activity, responseString + "    ");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        APIModel.handleFailure(activity, statusCode, responseString, new APIModel.RefreshTokenListener() {
                            @Override
                            public void onRefresh() {
                                accept();
                            }
                        });
                        break;
                }
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                Log.e("response", responseString);
                Utilities.toastySuccess(activity, "تم التنفيذ بنجاح");
                activity.finish();
            }

            @Override
            public void onStart() {
                super.onStart();
                Dialogs.showLoading(activity, loadingDialog);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                Dialogs.dismissLoading(loadingDialog);
            }
        });
    }
}