package com.example.zone.viewModels;

import androidx.databinding.ObservableField;

import com.example.zone.dataModel.NotificationModel;


public class ItemNotificationViewModel {
    public ObservableField<String> title = new ObservableField<>();
    public ObservableField<String> body = new ObservableField<>();
    public ObservableField<String> date = new ObservableField<>();

    public ItemNotificationViewModel(NotificationModel.ResultBean.UserBean notifiction) {
        title.set(notifiction.getNoticTitle());
        body.set(notifiction.getNoticMssge());
        date.set(notifiction.getNoticTime() +"  "+ notifiction.getNoticDate());

    }

}