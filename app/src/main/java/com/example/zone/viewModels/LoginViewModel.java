package com.example.zone.viewModels;


import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.widget.Toast;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.example.zone.API.APIModel;
import com.example.zone.R;
import com.example.zone.SharedPreferences.LoginSession;
import com.example.zone.Utilities.Dialogs;
import com.example.zone.Utilities.IntentClass;
import com.example.zone.Utilities.LoadingDialog;
import com.example.zone.Utilities.Utilities;
import com.example.zone.dataModel.UserModel;
import com.example.zone.ui.activities.LoginActivity;
import com.example.zone.ui.activities.MainActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;



/**
 * Created by hp on 2/4/2019.
 */

public class LoginViewModel {

    public ObservableField<String> usernameObservable = new ObservableField<>("");
    public ObservableField<String> usernameErrorObservable = new ObservableField<>("");
    public ObservableField<String> passwordObservable = new ObservableField<>("");
    public ObservableField<String> passwordErrorObservable = new ObservableField<>("");
    public ObservableBoolean isMasked = new ObservableBoolean(false);
    private LoginActivity activity;

    public LoginViewModel(LoginActivity activity) {
        this.activity = activity;
    }

    public void showPassword() {

        isMasked.set(!isMasked.get());

        if (!isMasked.get()) {
            // hide password
            
            activity.binding.EditTextPassword.setTransformationMethod(new PasswordTransformationMethod());


        } else {

            // show password

            activity.binding.EditTextPassword.setTransformationMethod(null);

        }

    }


    public void login() {
        Utilities.hideKeyboard(activity);
        usernameErrorObservable.set(null);
        passwordErrorObservable.set(null);
        String username = usernameObservable.get();
        String password = passwordObservable.get();
        boolean error = false;
        if (username == null || username.trim().isEmpty()) {
             Toast.makeText(activity, "Username is incorrect", Toast.LENGTH_SHORT).show();

            error = true;
        }
        if (password == null || password.trim().isEmpty()) {
            Toast.makeText(activity, "The password is incorrect", Toast.LENGTH_SHORT).show();
            error = true;
        }
        if (!error) {

            final LoadingDialog loadingDialog = new LoadingDialog();

            JSONObject jsonParams = new JSONObject();
            try {
                jsonParams.put("User_Name", username);
                jsonParams.put("User_Pass", password);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            APIModel.postMethod(activity, "Delivery/userlogin", jsonParams, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                    Log.e("response", responseString + "Error");
                    switch (statusCode) {
                        case 400:
                            try {
                                JSONObject jsonObject = new JSONObject(responseString);
                                if (jsonObject.has("result"))
                                    Utilities.toastyError(activity, jsonObject.getJSONObject("result").getString("user"));
                                else
                                    Utilities.toastyError(activity, responseString + "    ");
                            } catch (JSONException e2) {
                                e2.printStackTrace();
                            }
                            break;
                        default:
                            APIModel.handleFailure(activity, statusCode, responseString, new APIModel.RefreshTokenListener() {
                                @Override
                                public void onRefresh() {
                                    login();
                                }
                            });
                            break;
                    }
                }

                @Override
                public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                    Log.e("response", responseString);

                    try {
                        Type dataType = new TypeToken<UserModel>() {
                        }.getType();
                        UserModel data = new Gson().fromJson(responseString, dataType);

                        LoginSession.setUserData(activity, data);

                        IntentClass.goToActivityAndClear(activity, MainActivity.class, null, R.anim.fade_in_slow, R.anim.fade_out_slow);
                    }catch (Exception e){
                        try {
                            JSONObject jsonObject = new JSONObject(responseString);
                            if (jsonObject.has("result"))
                                Utilities.toastyError(activity, jsonObject.getJSONObject("result").getString("user"));
                            else
                                Utilities.toastyError(activity, responseString + "    ");
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                        }
                        e.printStackTrace();
                    }

                }

                @Override
                public void onStart() {
                    super.onStart();
                    Dialogs.showLoading(activity, loadingDialog);
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    Dialogs.dismissLoading(loadingDialog);
                }
            });
        }
    }

    public void back() {

        activity.finish();

    }

}
