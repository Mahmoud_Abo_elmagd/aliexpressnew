package com.example.zone.viewModels;

import android.app.Activity;
import android.os.Bundle;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.example.zone.SharedPreferences.LoginSession;
import com.example.zone.Utilities.IntentClass;
import com.example.zone.dataModel.OrdersModel;
import com.example.zone.ui.activities.OrderDetailsActivity;

public class ItemOrderViewModel {
    public ObservableField<String> status = new ObservableField<>();
    public ObservableField<String> orderID = new ObservableField<>();
    public ObservableField<String> name = new ObservableField<>();
    public ObservableField<String> phone = new ObservableField<>();
    public ObservableField<String> address = new ObservableField<>();
    public ObservableField<String> city = new ObservableField<>();
    public ObservableField<String> area = new ObservableField<>();
    public ObservableField<String> date = new ObservableField<>();
    public ObservableField<String> quantity = new ObservableField<>();
    public ObservableField<String> fee = new ObservableField<>();
    public ObservableField<String> orderFee = new ObservableField<>();
    public ObservableField<String> totalFee = new ObservableField<>();
    public ObservableField<String> senderPhone = new ObservableField<>();

    public ObservableBoolean isOutdoor = new ObservableBoolean(false);

    private Activity activity;

    public ObservableField<OrdersModel.ResultBean.UserBean> orderModelObservable = new ObservableField<>();

    private boolean isFromSearch;

    public ItemOrderViewModel(Activity activity, OrdersModel.ResultBean.UserBean report, boolean isFromSearch) {
        this.activity = activity;

        this.isFromSearch = isFromSearch;

        orderModelObservable.set(report);

        status.set(report.getStat_App_Nm() + "");
        orderID.set(report.getPolessa_No() + "");
        name.set(report.getClient_Name());
        phone.set(report.getClient_Phone());
        senderPhone.set("هاتف الراسل" + ":- " + report.getSender_Phone_No());
        address.set("العنوان" + ":- " + report.getClient_Address());
        city.set("المدينة" + ":- " + report.getCity_L_Nm());
        area.set("المنطقة" + ":- " + report.getArea_L_Nm());
        date.set(report.getDelivery_Date());
        quantity.set(report.getOrd_Quantity() + "");
        fee.set(report.getOrd_Total_Price() + "");
        orderFee.set(report.getOrd_Ship_Client() + "");
        totalFee.set(report.getOrd_Final_Price() + "");
    }

    public void call(int type) {
        if (type == 1)
            IntentClass.goTodialPhoneNumber(activity, phone.get());
        else
            IntentClass.goTodialPhoneNumber(activity, orderModelObservable.get().getSender_Phone_No());
    }

    public void itemClick() {
        Bundle bundle = new Bundle();
        bundle.putSerializable("orderModel", orderModelObservable.get());
        bundle.putBoolean("isFromSearch", isFromSearch);
        IntentClass.goToActivity(activity, OrderDetailsActivity.class, bundle);
    }
}