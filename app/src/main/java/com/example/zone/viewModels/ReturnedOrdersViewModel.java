package com.example.zone.viewModels;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;

import androidx.databinding.ObservableBoolean;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.zone.API.APIModel;
import com.example.zone.SharedPreferences.LoginSession;
import com.example.zone.Utilities.Dialogs;
import com.example.zone.Utilities.LoadingDialog;
import com.example.zone.Utilities.Utilities;
import com.example.zone.dataModel.OrdersModel;
import com.example.zone.dataModel.ReceiveModel;
import com.example.zone.ui.activities.RecieveOrdersActivity;
import com.example.zone.ui.activities.ReturnedOrdersActivity;
import com.example.zone.ui.adapter.OrdersAdapter;
import com.example.zone.ui.adapter.ReceiveOrdersAdapter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;

public class ReturnedOrdersViewModel {

    private ReturnedOrdersActivity activity;

    public ObservableBoolean showNoProductsObservable = new ObservableBoolean(false);

    public boolean isSearch = false;

    public String searchTxt = "";

    private int status;

    public ReturnedOrdersViewModel(ReturnedOrdersActivity activity, int status) {
        this.activity = activity;
        this.status = status;

        getData(1);
    }

    public void getData(final int page) {

        isSearch = false;

        final LoadingDialog loadingDialog = new LoadingDialog();

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("StatusID", status);
            jsonParams.put("DeliveryID", LoginSession.getUserData(activity).getResult().getUser().getEmployeeID());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        APIModel.postMethod(activity, "Delivery/returnorderslst?pag=" + page, jsonParams, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                Log.e("response", responseString + "Error");
                showNoProductsObservable.set(true);
                switch (statusCode) {
                    case 400:
                        try {
                            JSONObject jsonObject = new JSONObject(responseString);
                            if (jsonObject.has("error"))
                                Utilities.toastyError(activity, jsonObject.getJSONObject("error").getString("Message"));
                            else
                                Utilities.toastyError(activity, responseString + "    ");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        APIModel.handleFailure(activity, statusCode, responseString, new APIModel.RefreshTokenListener() {
                            @Override
                            public void onRefresh() {
                                getData(page);
                            }
                        });
                        break;
                }
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                Log.e("response", responseString);

                Type dataType = new TypeToken<OrdersModel>() {
                }.getType();
                OrdersModel data = new Gson().fromJson(responseString, dataType);

                if (data.getResult().getUser().size() > 0) {
                    activity.isLastPage = false;
                    showNoProductsObservable.set(false);
                    activity.updateRecycler(data.getResult().getUser());
                } else {
                    activity.isLastPage = true;
                    if (activity.nextPage == 1)
                        showNoProductsObservable.set(true);
                }

                activity.nextPage += 1;

            }

            @Override
            public void onStart() {
                super.onStart();
                Dialogs.showLoading(activity, loadingDialog);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                Dialogs.dismissLoading(loadingDialog);
            }
        });
    }

    public void getData(final String search, final int page) {

        isSearch = true;

        final LoadingDialog loadingDialog = new LoadingDialog();

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("Client_Name", search);
            jsonParams.put("DeliveryID", LoginSession.getUserData(activity).getResult().getUser().getEmployeeID());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        APIModel.postMethod(activity, "Delivery/searchreturnorderslst?pag=" + page, jsonParams, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                Log.e("response", responseString + "Error");
                showNoProductsObservable.set(true);
                switch (statusCode) {
                    case 400:
                        try {
                            JSONObject jsonObject = new JSONObject(responseString);
                            if (jsonObject.has("error"))
                                Utilities.toastyError(activity, jsonObject.getJSONObject("error").getString("Message"));
                            else
                                Utilities.toastyError(activity, responseString + "    ");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        APIModel.handleFailure(activity, statusCode, responseString, new APIModel.RefreshTokenListener() {
                            @Override
                            public void onRefresh() {
                                getData(search, page);
                            }
                        });
                        break;
                }
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                Log.e("response", responseString);

                Type dataType = new TypeToken<OrdersModel>() {
                }.getType();
                OrdersModel data = new Gson().fromJson(responseString, dataType);

                if (data.getResult().getUser().size() > 0) {
                    activity.isLastPage = false;
                    showNoProductsObservable.set(false);
                    activity.updateRecycler(data.getResult().getUser());
                } else {
                    activity.isLastPage = true;
                    if (activity.nextPage == 1)
                        showNoProductsObservable.set(true);
                }

                activity.nextPage += 1;

            }

            @Override
            public void onStart() {
                super.onStart();
                Dialogs.showLoading(activity, loadingDialog);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                Dialogs.dismissLoading(loadingDialog);
            }
        });
    }

    public TextWatcher getSearchText() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(final Editable editable) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        activity.nextPage = 1;
                        activity.isLastPage = false;
                        activity.listData.clear();
                        activity.returnedOrdersAdapter.notifyDataSetChanged();

                        isSearch = true;

                        if (editable.toString().length() > 0){

                            searchTxt = editable.toString();

                            getData(searchTxt, 1);
                        }else {
                            getData("", 1);
                        }


                    }
                }, 2000);
            }
        };
    }

    public void back() {
        activity.finish();
    }
}