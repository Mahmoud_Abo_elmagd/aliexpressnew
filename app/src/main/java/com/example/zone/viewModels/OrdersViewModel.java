package com.example.zone.viewModels;

import android.app.Activity;
import android.os.Bundle;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableFloat;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.zone.BR;
import com.example.zone.R;
import com.example.zone.SharedPreferences.ConfigurationFile;
import com.example.zone.ui.adapter.ViewPagerAdapter;
import com.example.zone.ui.fragments.OrdersFragment;

public class OrdersViewModel extends BaseObservable {
    public ObservableFloat rotationYObservable = new ObservableFloat(0);

    private Activity activity;
    private ViewPager viewPager;
    private FragmentManager fragmentManager;

    public ViewPagerAdapter adapter;

    private OrdersFragment dayFragment;
    private OrdersFragment monthFragment;
    private OrdersFragment allFragment;

    private int status;

    public OrdersViewModel(Activity activity, int status, ViewPager viewPager, FragmentManager fragmentManager) {
        this.activity = activity;
        this.status = status;
        this.viewPager = viewPager;
        this.fragmentManager = fragmentManager;

        dayFragment = new OrdersFragment(1, status);
        monthFragment = new OrdersFragment(2, status);
        allFragment = new OrdersFragment(0, status);

        if (ConfigurationFile.getCurrentLanguage(activity).equals("ar"))
            rotationYObservable.set(180);
        createViewPager();
    }

    private void createViewPager() {
        adapter = new ViewPagerAdapter(fragmentManager);
        adapter.addFragment(dayFragment, activity.getString(R.string.today));
        adapter.addFragment(monthFragment, activity.getString(R.string.month));
        adapter.addFragment(allFragment, activity.getString(R.string.all));

        notifyPropertyChanged(BR.pagerAdapter);

        Bundle bundle = new Bundle();
        bundle.putInt("status", status);
        dayFragment.setArguments(bundle);

        Bundle bundle2 = new Bundle();
        bundle.putInt("status", status);
        monthFragment.setArguments(bundle2);

        Bundle bundle3 = new Bundle();
        bundle.putInt("status", status);
        allFragment.setArguments(bundle3);
    }

    @Bindable
    public PagerAdapter getPagerAdapter() {
        return adapter;
    }

    public void back() {
        activity.onBackPressed();
    }

}