package com.example.zone.viewModels;

import android.util.Log;

import androidx.databinding.ObservableBoolean;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.zone.API.APIModel;
import com.example.zone.SharedPreferences.LoginSession;
import com.example.zone.Utilities.Dialogs;
import com.example.zone.Utilities.LoadingDialog;
import com.example.zone.Utilities.Utilities;
import com.example.zone.dataModel.NotificationModel;
import com.example.zone.ui.activities.NotificationActivity;
import com.example.zone.ui.adapter.NotificationsAdapter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;

public class NotificationsViewModel {

    private NotificationActivity activity;

    public ObservableBoolean showNoProductsObservable = new ObservableBoolean(false);

    public NotificationsViewModel(NotificationActivity activity) {
        this.activity = activity;

        getData(1);
    }

    public void getData(int page) {
        final LoadingDialog loadingDialog = new LoadingDialog();

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("Employee_ID", LoginSession.getUserData(activity).getResult().getUser().getEmployeeID());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        APIModel.postMethod(activity, "Delivery/getnotifcation?pag=" + page, jsonParams, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                Log.e("response", responseString + "Error");
                showNoProductsObservable.set(true);
                switch (statusCode) {
                    case 400:
                        try {
                            JSONObject jsonObject = new JSONObject(responseString);
                            if (jsonObject.has("error"))
                                Utilities.toastyError(activity, jsonObject.getJSONObject("error").getString("Message"));
                            else
                                Utilities.toastyError(activity, responseString + "    ");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        APIModel.handleFailure(activity, statusCode, responseString, new APIModel.RefreshTokenListener() {
                            @Override
                            public void onRefresh() {
                                getData(1);
                            }
                        });
                        break;
                }
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                Log.e("response", responseString);

                Type dataType = new TypeToken<NotificationModel>() {
                }.getType();
                NotificationModel data = new Gson().fromJson(responseString, dataType);

                if (data.getResult().getUser().size() > 0) {

                    NotificationsAdapter notificationsAdapter = new NotificationsAdapter(activity, data.getResult().getUser());
                    activity.binding.recyclerOrders.setLayoutManager(new LinearLayoutManager(activity));
                    activity.binding.recyclerOrders.setAdapter(notificationsAdapter);
                } else {
                    showNoProductsObservable.set(true);
                }

            }

            @Override
            public void onStart() {
                super.onStart();
                Dialogs.showLoading(activity, loadingDialog);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                Dialogs.dismissLoading(loadingDialog);
            }
        });
    }

    public void back() {
        activity.finish();
    }
}