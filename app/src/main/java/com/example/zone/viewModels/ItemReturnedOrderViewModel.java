package com.example.zone.viewModels;

import android.app.Activity;
import android.util.Log;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.example.zone.API.APIModel;
import com.example.zone.Utilities.Dialogs;
import com.example.zone.Utilities.IntentClass;
import com.example.zone.Utilities.LoadingDialog;
import com.example.zone.Utilities.Utilities;
import com.example.zone.dataModel.OrdersModel;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

public class ItemReturnedOrderViewModel {
    public ObservableField<String> status = new ObservableField<>();
    public ObservableField<String> orderID = new ObservableField<>();
    public ObservableField<String> name = new ObservableField<>();
    public ObservableField<String> phone = new ObservableField<>();
    public ObservableField<String> address = new ObservableField<>();
    public ObservableField<String> city = new ObservableField<>();
    public ObservableField<String> area = new ObservableField<>();
    public ObservableField<String> date = new ObservableField<>();
    public ObservableField<String> quantity = new ObservableField<>();
    public ObservableField<String> fee = new ObservableField<>();
    public ObservableField<String> orderFee = new ObservableField<>();
    public ObservableField<String> totalFee = new ObservableField<>();
    public ObservableField<String> senderPhone = new ObservableField<>();

    public ObservableBoolean isOutdoor = new ObservableBoolean(false);

    private Activity activity;

    public ObservableField<OrdersModel.ResultBean.UserBean> orderModelObservable = new ObservableField<>();

    public ItemReturnedOrderViewModel(Activity activity, OrdersModel.ResultBean.UserBean report) {
        this.activity = activity;

        orderModelObservable.set(report);

        status.set(report.getStat_App_Nm() + "");
        orderID.set(report.getPolessa_No() + "");
        name.set(report.getClient_Name());
        phone.set(report.getClient_Phone());
        senderPhone.set("هاتف الراسل" + ":- " + report.getSender_Phone_No());
        address.set("العنوان" + ":- " + report.getClient_Address());
        city.set("المدينة" + ":- " + report.getCity_L_Nm());
        area.set("المنطقة" + ":- " + report.getArea_L_Nm());
        date.set(report.getDelivery_Date());
        quantity.set(report.getOrd_Quantity() + "");
        fee.set(report.getOrd_Total_Price() + "");
        orderFee.set(report.getOrd_Ship_Client() + "");
        totalFee.set(report.getOrd_Final_Price() + "");
    }

    public void call(int type) {
        if (type == 1)
            IntentClass.goTodialPhoneNumber(activity, phone.get());
        else
            IntentClass.goTodialPhoneNumber(activity, orderModelObservable.get().getSender_Phone_No());
    }

    public void accept() {
        final LoadingDialog loadingDialog = new LoadingDialog();

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("RecordSerial", orderModelObservable.get().getRecordSerial());
            jsonParams.put("OrderSheetID", orderModelObservable.get().getOrderSheetID());
            jsonParams.put("DeliveryID", orderModelObservable.get().getDeliveryID());
            jsonParams.put("ClientID", orderModelObservable.get().getClientID());
            jsonParams.put("StatusID", 11);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        APIModel.postMethod(activity, "Delivery/returnaction", jsonParams, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                Log.e("response", responseString + "Error");
                switch (statusCode) {
                    case 400:
                        try {
                            JSONObject jsonObject = new JSONObject(responseString);
                            if (jsonObject.has("error"))
                                Utilities.toastyError(activity, jsonObject.getJSONObject("error").getString("Message"));
                            else
                                Utilities.toastyError(activity, responseString + "    ");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        APIModel.handleFailure(activity, statusCode, responseString, new APIModel.RefreshTokenListener() {
                            @Override
                            public void onRefresh() {
                                accept();
                            }
                        });
                        break;
                }
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                Log.e("response", responseString);
                Utilities.toastySuccess(activity, "تم التنفيذ بنجاح");
                activity.finish();
            }

            @Override
            public void onStart() {
                super.onStart();
                Dialogs.showLoading(activity, loadingDialog);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                Dialogs.dismissLoading(loadingDialog);
            }
        });
    }

}