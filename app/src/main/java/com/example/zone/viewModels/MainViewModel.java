package com.example.zone.viewModels;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.example.zone.API.APIModel;
import com.example.zone.SharedPreferences.ConfigurationFile;
import com.example.zone.SharedPreferences.LoginSession;
import com.example.zone.Utilities.Dialogs;
import com.example.zone.Utilities.IntentClass;
import com.example.zone.Utilities.LoadingDialog;
import com.example.zone.Utilities.Utilities;
import com.example.zone.dataModel.OrderCountModel;
import com.example.zone.dataModel.PermissionModel;
import com.example.zone.ui.activities.MainActivity;
import com.example.zone.ui.activities.MapActivity;
import com.example.zone.ui.activities.NotificationActivity;
import com.example.zone.ui.activities.OrdersActivity;
import com.example.zone.ui.activities.QRActivity;
import com.example.zone.ui.activities.RecieveOrdersActivity;
import com.example.zone.ui.activities.ReturnedOrdersActivity;
import com.example.zone.ui.activities.SearchActivity;
import com.example.zone.ui.fragments.LogOutBottomSheet;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;


/**
 * Created by hp on 3/18/2019.
 */

public class MainViewModel {

    // titles
    public ObservableField<String> balanceTxt = new ObservableField<>();
    public ObservableField<String> collectedTxt = new ObservableField<>();
    public ObservableField<String> uncollectedTxt = new ObservableField<>();
    public ObservableField<String> pickupTxt = new ObservableField<>();
    public ObservableField<String> deliveringTxt = new ObservableField<>();
    public ObservableField<String> returnedTxt = new ObservableField<>();
    public ObservableField<String> partialTxt = new ObservableField<>();
    public ObservableField<String> postponedTxt = new ObservableField<>();
    public ObservableField<String> canceledTxt = new ObservableField<>();
    public ObservableField<String> deliveredTxt = new ObservableField<>();
    public ObservableField<String> allTxt = new ObservableField<>();
    public ObservableField<String> mapTxt = new ObservableField<>();

    // values
    public ObservableField<String> balanceVal = new ObservableField<>();
    public ObservableField<String> collectedVal = new ObservableField<>();
    public ObservableField<String> uncollectedVal = new ObservableField<>();
    public ObservableField<String> pickupVal = new ObservableField<>();
    public ObservableField<String> deliveringVal = new ObservableField<>();
    public ObservableField<String> returnedVal = new ObservableField<>();
    public ObservableField<String> partialVal = new ObservableField<>();
    public ObservableField<String> postponedVal = new ObservableField<>();
    public ObservableField<String> canceledVal = new ObservableField<>();
    public ObservableField<String> deliveredVal = new ObservableField<>();
    public ObservableField<String> allVal = new ObservableField<>();


    // visibility
    public ObservableBoolean balanceFlag = new ObservableBoolean(false);
    public ObservableBoolean collectedFlag = new ObservableBoolean(false);
    public ObservableBoolean uncollectedFlag = new ObservableBoolean(false);
    public ObservableBoolean pickupFlag = new ObservableBoolean(false);
    public ObservableBoolean deliveringFlag = new ObservableBoolean(false);
    public ObservableBoolean returnedFlag = new ObservableBoolean(false);
    public ObservableBoolean partialFlag = new ObservableBoolean(false);
    public ObservableBoolean postponedFlag = new ObservableBoolean(false);
    public ObservableBoolean canceledFlag = new ObservableBoolean(false);
    public ObservableBoolean deliveredFlag = new ObservableBoolean(false);
    public ObservableBoolean allFlag = new ObservableBoolean(false);
    public ObservableBoolean mapLocation = new ObservableBoolean(false);


    private MainActivity activity;

    public MainViewModel(MainActivity activity) {
        this.activity = activity;
    }


    public void settingClick() {
        LogOutBottomSheet logOutBottomSheet = new LogOutBottomSheet(activity);
        logOutBottomSheet.show(activity.getSupportFragmentManager(), "tag");
    }

    public void searchClick() {
        Bundle bundle = new Bundle();
        bundle.putBoolean("fromHome", false);
        IntentClass.goToActivity(activity, SearchActivity.class, bundle);
    }

    public void notificationClick() {
        IntentClass.goToActivity(activity, NotificationActivity.class, null);
    }

    public void qrClick() {
        Bundle bundle = new Bundle();
        bundle.putBoolean("fromHome", true);
        IntentClass.goToActivity(activity, SearchActivity.class, bundle);
    }

    public void mapClick() {
        if ( checkLocationPermission()){
            Bundle bundle = new Bundle();
            bundle.putString("title", mapTxt.get());
            IntentClass.goToActivity(activity, MapActivity.class, bundle);
        }
    }

    private boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        99);
            } else {
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        99);
            }
            return false;
        }else {
            return true;
        }
    }

    public void returnedClick() {
        Bundle bundle = new Bundle();
        bundle.putString("title", returnedTxt.get());
        bundle.putInt("status", 11);
        IntentClass.goToActivity(activity, ReturnedOrdersActivity.class, bundle);
    }



    public void deliveringClick() {
        Bundle bundle = new Bundle();
        bundle.putString("title", deliveringTxt.get());
        bundle.putInt("status", 3);
        IntentClass.goToActivity(activity, OrdersActivity.class, bundle);
    }

    public void deliveredClick() {
        Bundle bundle = new Bundle();
        bundle.putString("title", deliveredTxt.get());
        bundle.putInt("status", 4);
        IntentClass.goToActivity(activity, OrdersActivity.class, bundle);
    }

    public void canceledClick() {
        Bundle bundle = new Bundle();
        bundle.putString("title", canceledTxt.get());
        bundle.putInt("status", 5);
        IntentClass.goToActivity(activity, OrdersActivity.class, bundle);
    }

    public void postponedClick() {
        Bundle bundle = new Bundle();
        bundle.putString("title", postponedTxt.get());
        bundle.putInt("status", 6);
        IntentClass.goToActivity(activity, OrdersActivity.class, bundle);
    }

    public void partialClick() {
        Bundle bundle = new Bundle();
        bundle.putString("title", partialTxt.get());
        bundle.putInt("status", 7);
        IntentClass.goToActivity(activity, OrdersActivity.class, bundle);
    }

    public void pickUpClick() {
        Bundle bundle = new Bundle();
        bundle.putString("title", pickupTxt.get());
        IntentClass.goToActivity(activity, RecieveOrdersActivity.class, bundle);
    }

    private void getOrdersCount() {
        //final LoadingDialog loadingDialog = new LoadingDialog();

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("Dash_EmployeeID", LoginSession.getUserData(activity).getResult().getUser().getEmployeeID());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        APIModel.postMethod(activity, "Delivery/appdashboard", jsonParams, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                Log.e("response", responseString + "Error");
                switch (statusCode) {
                    case 400:
                        try {
                            JSONObject jsonObject = new JSONObject(responseString);
                            if (jsonObject.has("error"))
                                Utilities.toastyError(activity, jsonObject.getJSONObject("error").getString("Message"));
                            else
                                Utilities.toastyError(activity, responseString + "    ");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        APIModel.handleFailure(activity, statusCode, responseString, new APIModel.RefreshTokenListener() {
                            @Override
                            public void onRefresh() {
                                getOrdersCount();
                            }
                        });
                        break;
                }
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                Log.e("response", responseString);

                Type dataType = new TypeToken<OrderCountModel>() {
                }.getType();
                OrderCountModel data = new Gson().fromJson(responseString, dataType);

                balanceVal.set(String.valueOf(data.getResult().getUser().getDashEmpbalance()));
                collectedVal.set(String.valueOf(data.getResult().getUser().getDashEmpcollected()));
                uncollectedVal.set(String.valueOf(data.getResult().getUser().getDashEmpnotcollected()));
                pickupVal.set(String.valueOf(data.getResult().getUser().getDashRecivereq()));
                deliveringVal.set(String.valueOf(data.getResult().getUser().getDashInshipping()));
                returnedVal.set(String.valueOf(data.getResult().getUser().getDashReturned()));
                partialVal.set(String.valueOf(data.getResult().getUser().getDashPartdelevired()));
                postponedVal.set(String.valueOf(data.getResult().getUser().getDashPostponed()));
                canceledVal.set(String.valueOf(data.getResult().getUser().getDashCanceled()));
                deliveredVal.set(String.valueOf(data.getResult().getUser().getDashDelevired()));
                allVal.set(String.valueOf(data.getResult().getUser().getDashAllorders()));

//                activity.anyChartTest();
//
//                ConfigurationFile.setCurrentLanguage(activity, "ar");

            }

            @Override
            public void onStart() {
                super.onStart();
               // Dialogs.showLoading(activity, loadingDialog);
            }

            @Override
            public void onFinish() {
                super.onFinish();
              //  Dialogs.dismissLoading(loadingDialog);
            }
        });
    }

    public void getUserPermission() {
        final LoadingDialog loadingDialog = new LoadingDialog();

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("Role_ID", LoginSession.getUserData(activity).getResult().getUser().getRoleID());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        APIModel.postMethod(activity, "Delivery/userpermision", jsonParams, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                Log.e("response", responseString + "Error");
                switch (statusCode) {
                    case 400:
                        try {
                            JSONObject jsonObject = new JSONObject(responseString);
                            if (jsonObject.has("error"))
                                Utilities.toastyError(activity, jsonObject.getJSONObject("error").getString("Message"));
                            else
                                Utilities.toastyError(activity, responseString + "    ");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        APIModel.handleFailure(activity, statusCode, responseString, new APIModel.RefreshTokenListener() {
                            @Override
                            public void onRefresh() {
                                getUserPermission();
                            }
                        });
                        break;
                }
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                Log.e("response", responseString);

                Type dataType = new TypeToken<PermissionModel>() {
                }.getType();
                PermissionModel data = new Gson().fromJson(responseString, dataType);

                for (int index = 0; index < data.getResult().getUser().size(); index++) {

                    if (data.getResult().getUser().get(index).getScreenID() == 1) {
                        balanceFlag.set(data.getResult().getUser().get(index).isPermStatus());
                        balanceTxt.set(data.getResult().getUser().get(index).getScreenName());
                    } else if (data.getResult().getUser().get(index).getScreenID() == 2) {
                        collectedFlag.set(data.getResult().getUser().get(index).isPermStatus());
                        collectedTxt.set(data.getResult().getUser().get(index).getScreenName());
                    } else if (data.getResult().getUser().get(index).getScreenID() == 3) {
                        uncollectedFlag.set(data.getResult().getUser().get(index).isPermStatus());
                        uncollectedTxt.set(data.getResult().getUser().get(index).getScreenName());
                    } else if (data.getResult().getUser().get(index).getScreenID() == 4) {
                        pickupFlag.set(data.getResult().getUser().get(index).isPermStatus());
                        pickupTxt.set(data.getResult().getUser().get(index).getScreenName());
                    } else if (data.getResult().getUser().get(index).getScreenID() == 5) {
                        deliveringFlag.set(data.getResult().getUser().get(index).isPermStatus());
                        deliveringTxt.set(data.getResult().getUser().get(index).getScreenName());
                    } else if (data.getResult().getUser().get(index).getScreenID() == 6) {
                        returnedFlag.set(data.getResult().getUser().get(index).isPermStatus());
                        returnedTxt.set(data.getResult().getUser().get(index).getScreenName());
                    } else if (data.getResult().getUser().get(index).getScreenID() == 7) {
                        partialFlag.set(data.getResult().getUser().get(index).isPermStatus());
                        partialTxt.set(data.getResult().getUser().get(index).getScreenName());
                    } else if (data.getResult().getUser().get(index).getScreenID() == 8) {
                        postponedFlag.set(data.getResult().getUser().get(index).isPermStatus());
                        postponedTxt.set(data.getResult().getUser().get(index).getScreenName());
                    } else if (data.getResult().getUser().get(index).getScreenID() == 9) {
                        canceledFlag.set(data.getResult().getUser().get(index).isPermStatus());
                        canceledTxt.set(data.getResult().getUser().get(index).getScreenName());
                    } else if (data.getResult().getUser().get(index).getScreenID() == 10) {
                        deliveredFlag.set(data.getResult().getUser().get(index).isPermStatus());
                        deliveredTxt.set(data.getResult().getUser().get(index).getScreenName());
                    } else if (data.getResult().getUser().get(index).getScreenID() == 11) {
                        allFlag.set(data.getResult().getUser().get(index).isPermStatus());
                        allTxt.set(data.getResult().getUser().get(index).getScreenName());
                    }else if (data.getResult().getUser().get(index).getScreenID() == 12) {
                        mapLocation.set(data.getResult().getUser().get(index).isPermStatus());
                        mapTxt.set(data.getResult().getUser().get(index).getScreenName());
                    }

                }
            }

            @Override
            public void onStart() {
                super.onStart();
                Dialogs.showLoading(activity, loadingDialog);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                Dialogs.dismissLoading(loadingDialog);
            }
        });
    }

    public void onResume() {
        getOrdersCount();
    }


    public void updateFCM(final Context context) {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.e("token", "Fetching FCM registration token failed", task.getException());
                            return;
                        }

                        // Get new FCM registration token
                        String token = task.getResult();

                        updateFCMRequest(context, token);

                        // Log and toast
                        Log.e("token", token);
                        //Toast.makeText(MainActivity.this, token, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void updateFCMRequest(final Context context, final String token) {

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("UserUID", LoginSession.getUserData(context).getResult().getUser().getUserUID());
            jsonParams.put("User_Fcm", token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        APIModel.postMethod(activity, "Delivery/updatefcm", jsonParams, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                Log.e("response", responseString + "Error");
                switch (statusCode) {
                    case 400:
                        try {
                            JSONObject jsonObject = new JSONObject(responseString);
                            if (jsonObject.has("result"))
                                Utilities.toastyError(activity, jsonObject.getJSONObject("result").getString("user"));
                            else
                                Utilities.toastyError(activity, responseString + "    ");
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                        }
                        break;
                    default:
                        APIModel.handleFailure(activity, statusCode, responseString, new APIModel.RefreshTokenListener() {
                            @Override
                            public void onRefresh() {
                                updateFCMRequest(context, token);
                            }
                        });
                        break;
                }
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                Log.e("response", responseString);
            }

            @Override
            public void onStart() {
                super.onStart();
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        });
    }

    public void updateLocation(final Context context, final LatLng latLng) {

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("UserUID", LoginSession.getUserData(context).getResult().getUser().getUserUID());
            jsonParams.put("User_Lat", latLng.latitude);
            jsonParams.put("User_Lng", latLng.longitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        APIModel.postMethod(activity, "Delivery/updatelocation", jsonParams, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                Log.e("response", responseString + "Error");
                switch (statusCode) {
                    case 400:
                        try {
                            JSONObject jsonObject = new JSONObject(responseString);
                            if (jsonObject.has("result"))
                                Utilities.toastyError(activity, jsonObject.getJSONObject("result").getString("user"));
                            else
                                Utilities.toastyError(activity, responseString + "    ");
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                        }
                        break;
                    default:
                        APIModel.handleFailure(activity, statusCode, responseString, new APIModel.RefreshTokenListener() {
                            @Override
                            public void onRefresh() {
                                updateLocation(context, latLng);
                            }
                        });
                        break;
                }
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                Log.e("response", responseString);
            }

            @Override
            public void onStart() {
                super.onStart();
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        });
    }

}
