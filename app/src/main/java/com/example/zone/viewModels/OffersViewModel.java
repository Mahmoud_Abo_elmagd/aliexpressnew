package com.example.zone.viewModels;

import android.util.Log;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableFloat;

import com.example.zone.API.APIModel;
import com.example.zone.SharedPreferences.ConfigurationFile;
import com.example.zone.SharedPreferences.LoginSession;
import com.example.zone.Utilities.Dialogs;
import com.example.zone.Utilities.LoadingDialog;
import com.example.zone.Utilities.Utilities;
import com.example.zone.dataModel.OrdersModel;
import com.example.zone.ui.fragments.OrdersFragment;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;


public class OffersViewModel {

    private OrdersFragment activity;

    public ObservableBoolean showNoProductsObservable = new ObservableBoolean(false);
    public ObservableFloat rotationYObservable = new ObservableFloat(0);

    private int fltr, status;

    public OffersViewModel(OrdersFragment activity, int fltr, int status) {
        this.activity = activity;
        this.fltr = fltr;
        this.status = status;

        if (ConfigurationFile.getCurrentLanguage(activity.getContext()).equals("ar"))
            rotationYObservable.set(180);

        getData(1);
    }

    public void getData(final int page) {
        final LoadingDialog loadingDialog = new LoadingDialog();

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("StatusID", status);
            jsonParams.put("DeliveryID", LoginSession.getUserData(activity.getActivity()).getResult().getUser().getEmployeeID());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        APIModel.postMethod(activity.getActivity(), "Delivery/orderslst?fltr=" + fltr + "&pag=" + page, jsonParams, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                Log.e("response", responseString + "Error");
                showNoProductsObservable.set(true);
                switch (statusCode) {
                    case 400:
                        try {
                            JSONObject jsonObject = new JSONObject(responseString);
                            if (jsonObject.has("error"))
                                Utilities.toastyError(activity.getActivity(), jsonObject.getJSONObject("error").getString("Message"));
                            else
                                Utilities.toastyError(activity.getActivity(), responseString + "    ");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        APIModel.handleFailure(activity.getActivity(), statusCode, responseString, new APIModel.RefreshTokenListener() {
                            @Override
                            public void onRefresh() {
                                getData(page);
                            }
                        });
                        break;
                }
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                Log.e("response", responseString);

                Type dataType = new TypeToken<OrdersModel>() {
                }.getType();
                OrdersModel data = new Gson().fromJson(responseString, dataType);

                if (data.getResult().getUser().size() > 0) {
                    activity.isLastPage = false;
                    showNoProductsObservable.set(false);
                    activity.updateRecycler(data.getResult().getUser());

                } else {
                    activity.isLastPage = true;
                    if (activity.nextPage == 1)
                        showNoProductsObservable.set(true);
                }

                activity.nextPage += 1;

            }

            @Override
            public void onStart() {
                super.onStart();
                Dialogs.showLoading(activity.getActivity(), loadingDialog);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                Dialogs.dismissLoading(loadingDialog);
            }
        });
    }
}