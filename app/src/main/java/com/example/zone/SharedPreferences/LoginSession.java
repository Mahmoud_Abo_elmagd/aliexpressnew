package com.example.zone.SharedPreferences;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.example.zone.dataModel.UserModel;
import com.example.zone.ui.activities.SplashActivity;
import com.google.gson.Gson;


/**
 * Created by MAHMOUD on 28/8/2020.
 */

public class LoginSession {
    private static final String USER_DATA_KEY = "userData";
    private static final String IS_LOGIN_KEY = "isLogin";
    private static final String ACCESS_TOKEN_KEY = "accessTokenKey";
    private static final String EXPIRE_KEY = "expireKey";
    private static SharedPreferences loginFile;


    private static void initLoginSharedPreference(Context context) {
        loginFile = context.getSharedPreferences("loginFile", Context.MODE_PRIVATE);
    }


    public static void setUserData(Activity activity, UserModel user) {
        initLoginSharedPreference(activity);
        SharedPreferences.Editor editor = loginFile.edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        editor.putString(USER_DATA_KEY, json);
        editor.putBoolean(IS_LOGIN_KEY, user != null);
        editor.apply();
    }

    public static UserModel getUserData(Context activity) {
        initLoginSharedPreference(activity);
        Gson gson = new Gson();
        String json = loginFile.getString(USER_DATA_KEY, "");
        UserModel userModel = gson.fromJson(json, UserModel.class);

        if (userModel != null)
            return userModel;

        return new UserModel();
    }

    public static boolean isLoggedIn(Activity activity) {
        initLoginSharedPreference(activity);
        return loginFile.getBoolean(IS_LOGIN_KEY, false);
    }

    public static boolean isLoggedIn(Context activity) {
        initLoginSharedPreference(activity);
        return loginFile.getBoolean(IS_LOGIN_KEY, false);
    }

    public static void setLoginKey(Activity activity) {
        initLoginSharedPreference(activity);
        SharedPreferences.Editor editor = loginFile.edit();
        editor.putBoolean(IS_LOGIN_KEY, false);
        editor.apply();
    }

    public static void setTokenData(Context activity, String accessToken, String expireKey) {
        initLoginSharedPreference(activity);
        SharedPreferences.Editor editor = loginFile.edit();
        editor.putString(ACCESS_TOKEN_KEY, accessToken);
        editor.putString(EXPIRE_KEY, expireKey);
        editor.apply();
    }

    public static String getAccessToken(Context activity) {
        initLoginSharedPreference(activity);
        return loginFile.getString(ACCESS_TOKEN_KEY, "");
    }

    public static String getExpire(Activity activity) {
        initLoginSharedPreference(activity);
        return loginFile.getString(EXPIRE_KEY, "1");
    }

    public static String getExpire(Context activity) {
        initLoginSharedPreference(activity);
        return loginFile.getString(EXPIRE_KEY, "1");
    }

    public static void clearData(Context activity) {
        initLoginSharedPreference(activity);
        SharedPreferences.Editor editor = loginFile.edit();
        editor.clear();
        editor.apply();
        Intent intent = new Intent(activity, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }
}