package com.example.zone.SharedPreferences;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;

import java.util.Locale;


/**
 * Created by MAHMOUD on 28/8/2020.
 */

public class ConfigurationFile {
    private static final String LANGUAGE_KEY = "languageKey";
    private static final String NOTIFICATIONS_KEY = "notificationsKey";

    private static SharedPreferences configFile;

    public static String getCurrentLanguage(Context context) {
        initConfigSharedPreference(context);
        return configFile.getString(LANGUAGE_KEY, Locale.getDefault().getLanguage());
    }

    public static void setCurrentLanguage(Context context, String language) {
        initConfigSharedPreference(context);

        if (language.equals(""))
            language = Locale.getDefault().getLanguage();

        try {
            Locale locale = new Locale(language);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            context.getResources().updateConfiguration(config, null);
        } catch (NullPointerException a) {
            a.printStackTrace();
        } catch (RuntimeException a) {
            a.printStackTrace();
        }

        SharedPreferences.Editor editor = configFile.edit();
        editor.putString(LANGUAGE_KEY, language);
        editor.apply();
    }

    public static void setNotificationStatus(Activity activity, boolean isActive) {
        initConfigSharedPreference(activity);
        SharedPreferences.Editor editor = configFile.edit();
        editor.putBoolean(NOTIFICATIONS_KEY, isActive);
        editor.apply();
    }

    public static boolean getNotificationStatus(Activity activity) {
        initConfigSharedPreference(activity);
        return configFile.getBoolean(NOTIFICATIONS_KEY, true);
    }

    public static boolean getNotificationStatus(Context activity) {
        initConfigSharedPreference(activity);
        return configFile.getBoolean(NOTIFICATIONS_KEY, true);
    }

    private static void initConfigSharedPreference(Context context) {
        configFile = context.getSharedPreferences("configFile", Context.MODE_PRIVATE);
    }
}
